# APOSTA #

Graphic tool used to represent prediction models for machine learning. User manual accessible [here](https://www.dropbox.com/s/bj471y3hy0nh1iw/Manual_de_usuario.pdf?dl=0) (currently only in Spanish).


###Dependencies:

####GUI library
* [PyQt 4.11.2](https://www.riverbankcomputing.com/news/)
**or**
[PySide 1.2.4](http://pyside.github.io/docs/pyside/)

####Plotting library:
* [matplotlib 1.4.2](http://matplotlib.org/1.4.3/index.html)