# Global configuration

# Sqlite BD path
db_path = "app/core/db/aposta.db"

# stopping_criterion_exec: represents the stopping criterion
# executable path
stopping_criterion_exec = "/home/diego/Corpora/stopping-criterion/bin/stoppingCriterion.pl"
#stopping_criterion_exec = ""
# stopping_criterion_dir: represents the directory which
# contains the cli perl program data for stopping criterion
# never forget to put the final slash for the directory path
stopping_criterion_dir = "/home/diego/TFG/code/aposta_repo/stopping-criterion/"
#stopping_criterion_dir = ""
# selection_task_exec: represents the stopping criterion
# executable path
selection_task_exec = "/home/diego/Corpora/selection-task/bin/selectionTask.pl"
#selection_task_exec = ""
# selection_task_dir: represents the directory which
# contains the cli perl program data for selection task
# never forget to put the final slash for the directory path
selection_task_dir = "/home/diego/TFG/code/aposta_repo/selection-task/"
#selection_task_dir = ""
# available_taggers: represents the list of available taggers for the system
available_taggers = (
    'fnTBL', 'lapos', 'MaxEnt', 'MBT',
    'Morfette', 'MrTagoo', 'MXPOST',
    'Stanford', 'SVMTool', 'TreeTagger',
    'TnT'
)

# available_corpus: represents the list of available corpus for the system
available_corpus = (
    'anCora', 'Frown', 'Penn'
)

# available_ancorss: represents the list of available ancors for the system
available_anchors = (
    'no-anchor', 'canonical'
)

# available_step_functions: represents the list of available step functions
available_step_functions = (
    'geometric',
    'arithmetic'
)


# starting_individual: represents the first individual that contains
# a trends-[individual_number].txt file
starting_individual = 3


# function_increment: represents the number of times X is incremented from
# the first individual to the last one
# for a trend function like: a * X **b + c
# example: from 5000 to 8000000, incrementing by X=4
function_increment = 1
