import os
import sys
import subprocess
import multiprocessing
from app import config
from app.core.aposta_adapter.run_training_interface import RunTrainingInterface


class RunTrainingAdapter(RunTrainingInterface):

    def __init__(self, training):
        self._training = training

    def _fork(self, cmd):
        # Configure the child processes environment
        os.chdir('/')
        os.setsid()
        os.umask(0)
        subprocess.Popen(cmd, shell=True)
        sys.exit(0)

    def run_training(self, num_process):
        working_dir = ""
        cmd = ""

        if self._training.get_adaptative_port():
            if os.path.isfile(config.selection_task_exec):
                cmd += "perl {}".format(config.selection_task_exec)

                cmd += " -p {}".format(self._training.get_adaptative_port())
                cmd += " -d {}".format(config.selection_task_dir)
            else:
                raise Exception("Check the configuration file. Missing or incorrect APOSTA"
                                " selectionTask executable.")
        else:
            if os.path.isfile(config.stopping_criterion_exec):
                cmd += "perl {}".format(config.stopping_criterion_exec)
                cmd += " -d {}".format(config.stopping_criterion_dir)
            else:
                raise Exception("Check the configuration file. Missing or incorrect APOSTA"
                                " stoppingCriterion executable.")

        cmd += " -c {}".format(self._training.get_corpus())
        cmd += " -t {}".format(self._training.get_tagger())
        cmd += " -n {}".format(num_process)
        cmd += " -k {}".format(self._training.get_kernel())
        if self._training.get_arithmetic_step():
            cmd += " -s {}".format(self._training.get_arithmetic_step())
        if self._training.get_geometric_factor():
            cmd += " -g {}".format(self._training.get_geometric_factor())

        cmd += " -l {}".format(self._training.get_convergence_threshold())
        cmd += " -a {}".format(self._training.get_anchor())
        cmd += " -v {},{},{}".format(self._training.get_verticality_threshold(),
                                     self._training.get_slow_down(),
                                     self._training.get_look_ahead()
                                     )

        cmd += " -m {}".format(self._training.get_max_indiv_size())

        cmd += " -u {}".format(self._training.get_id())

        p = multiprocessing.Process(target=self._fork, args=(cmd,))
        p.start()
        print cmd
