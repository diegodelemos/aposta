from app import config
from app.core.aposta_adapter.run_training_adapter import RunTrainingAdapter
from app.core.aposta_adapter.run_training_interface import RunTrainingInterface
from app.core.db.database import Database
from app.core.persistence.db_persistence.db_training_manager import DBTrainingManager


class RunTrainingProxy(RunTrainingInterface):

    def __init__(self, training):
        self._training = training
        db = Database(config.db_path)
        self._training_manager = DBTrainingManager(db)

    def _same_parameters_is_running(self):
        # check whether there is any training with the same parameters with the "started" state
        # if there is not (same parameters except convergence_threshold or max size individuals
        from app.core.core_facade import CoreFacade
        CoreFacade.refresh_started_trainings()
        started_trainings = self._training_manager.get_pending_trainings()
        for training in started_trainings:
            if training.get_path() == self._training.get_path() and training.get_state() == "started":
                return True

        return False

    def _training_already_exists(self):
        # make training persistent (BD)
        if self._training_manager.exist(self._training):
            return True
        else:
            return False

    def run_training(self, num_process):
        if self._same_parameters_is_running():
            raise Exception("Same parameters training is running")

        if self._training_already_exists():
            raise Exception("Training already exists")

        training_id, ok = self._training_manager.add(self._training)
        if ok:
            self._training.set_id(training_id)
            run_training_adapter = RunTrainingAdapter(self._training)
            try:
                run_training_adapter.run_training(num_process)
            except Exception, e:
                self._training_manager.remove(self._training)
                raise e

        else:
            raise Exception("There was an error while saving the training in the DB")
