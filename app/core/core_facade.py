from app.core.aposta_adapter.run_training_proxy import RunTrainingProxy

from app import config
from app.core.aposta_adapter.run_training_adapter import RunTrainingAdapter
from app.core.db.database import Database
from app.core.persistence.db_persistence.db_training_manager import DBTrainingManager
from app.core.persistence.db_persistence.db_trend_manager import DBTrendManager
from app.core.persistence.dir_tree_persistence.directory_tree_training_provider import DirectoryTreeTrainingProvider
from app.core.persistence.dir_tree_persistence.directory_tree_trend_provider import DirectoryTreeTrendProvider
from app.core.plot_data_generator import PlotDataGenerator


class CoreFacade:
    db = Database(config.db_path)

    @classmethod
    def get_available_trainings(cls):
        training_provider = DBTrainingManager(cls.db)
        trend_provider = DBTrendManager(cls.db)
        trainings = training_provider.get_all_trainings()
        for training in trainings:
            training.set_trend_provider(trend_provider)
        return trainings

    @classmethod
    def dump_dir_tree_to_db(cls):
        dir_tree_training_provider = DirectoryTreeTrainingProvider(config.stopping_criterion_dir)
        dir_tree_trend_provider = DirectoryTreeTrendProvider(config.stopping_criterion_dir)
        dir_tree_trainings = dir_tree_training_provider.get_all_trainings()
        # Trainings dump
        training_manager = DBTrainingManager(cls.db)
        training_manager.add_list(dir_tree_trainings)
        # Trends dump
        trend_manager = DBTrendManager(cls.db)
        for training in dir_tree_trainings:
            training.set_trend_provider(dir_tree_trend_provider)
            trend_manager.add_list(training)

    @classmethod
    def delete_all(cls):
        training_manager = DBTrainingManager(cls.db)
        training_manager.delete_all()

    @classmethod
    def delete_training(cls, training):
        training_manager = DBTrainingManager(cls.db)
        return training_manager.remove(training)

    @classmethod
    def refresh_started_trainings(cls):
        dir_tree_stoppingcriterion_training_provider = DirectoryTreeTrainingProvider(config.stopping_criterion_dir)
        dir_tree_selectiontask_training_provider = DirectoryTreeTrainingProvider(config.selection_task_dir)
        dir_tree_stoppingcriterion_trend_provider = DirectoryTreeTrendProvider(config.stopping_criterion_dir)
        dir_tree_selectiontask_trend_provider = DirectoryTreeTrendProvider(config.selection_task_dir)
        training_manager = DBTrainingManager(cls.db)
        trend_manager = DBTrendManager(cls.db)
        pending_trainings = training_manager.get_pending_trainings()

        for training in pending_trainings:
            if training.get_adaptative_port():
                state, ok = dir_tree_selectiontask_training_provider.is_ended_training(training)
                if ok:
                    training.set_trend_provider(dir_tree_selectiontask_trend_provider)
                    training.set_state(state)
                    # update training
                    if training_manager.update_training(training):
                        # add training trends to the DB if necessary
                        trend_manager.add_list(training)
            else:
                state, ok = dir_tree_stoppingcriterion_training_provider.is_ended_training(training)
                if ok:
                    training.set_trend_provider(dir_tree_stoppingcriterion_trend_provider)
                    training.set_state(state)
                    # update training
                    if training_manager.update_training(training):
                        # add training trends to the DB if necessary
                        trend_manager.add_list(training)

    @classmethod
    def run_training(cls, training, num_process):
        rtp = RunTrainingProxy(training)
        rtp.run_training(num_process)

    @staticmethod
    def rerun_training(training, num_process):
        rta = RunTrainingAdapter(training)
        rta.run_training(num_process)

    @staticmethod
    def get_trends_plot_data(training, trends):
        return PlotDataGenerator.get_trends_plot_data(training, trends)

    @staticmethod
    def get_training_derivatives_plot_data(training):
        return PlotDataGenerator.get_training_derivatives_plot_data(training)

    @staticmethod
    def get_training_point_cloud_plot_data(training):
        return PlotDataGenerator.get_training_point_cloud_plot_data(training)

    @staticmethod
    def get_residuals_inf_plot_data(training):
        return PlotDataGenerator.get_residuals_inf_plot_data(training)

    @staticmethod
    def get_residuals_sum_plot_data(training):
        return PlotDataGenerator.get_residuals_sum_plot_data(training)

    @staticmethod
    def get_residuals_diff_same_individual_plot_data(training):
        return PlotDataGenerator.get_residuals_diff_same_individual_plot_data(training)

    @staticmethod
    def get_residuals_diff_prev_individual_plot_data(training):
        return PlotDataGenerator.get_residuals_diff_prev_individual_plot_data(training)

    @staticmethod
    def get_asymptote_plot_data(training):
        return PlotDataGenerator.get_asymptote_plot_data(training)
