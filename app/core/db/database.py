import sys
import sqlite3


class Database:

    def __init__(self, db_name):
        self.db_name = db_name
        self.conn = None

    def get_connection(self):
        try:
            self.conn = sqlite3.connect(self.db_name)
            self.conn.row_factory = sqlite3.Row
        except sqlite3.Error, e:
            print "Error while connecting to the db {0} .\nError {1}".format(self.db_name, e.args[0])
            sys.exit(1)

        return self.conn

    def close_connection(self):
        self.conn.delete_plot_manager()
