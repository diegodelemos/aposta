--
-- start
--
DROP TABLE IF EXISTS Training;
DROP TABLE IF EXISTS Trend;
DROP TABLE IF EXISTS DiscreteValues;

--
-- Training
--
CREATE TABLE Training(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  path CHAR(70),
  anchor CHAR(30),
  convergence_threshold REAL,
  verticality_threshold REAL,
  slow_down REAL,
  look_ahead REAL,
  kernel INTEGER,
  arithmetic_step INTEGER,
  geometric_factor REAL,
  adaptative_port REAL,
  corpus CHAR(20),
  tagger CHAR(20),
  max_indiv_size INTEGER,
  state CHAR(20)
);

--
-- Trend
--
CREATE TABLE Trend(
  training_path CHAR(70) NOT NULL,
  individual_number INTEGER NOT NULL,
  a REAL,
  b REAL,
  c REAL,
  slope REAL,
  rmse REAL,
  PRIMARY KEY(training_path, individual_number),
  FOREIGN KEY(training_path) REFERENCES Training(path)
);

--
-- DiscreteValues
--
CREATE TABLE DiscreteValues(
  training_path CHAR(70) NOT NULL,
  actual_individual_number INTEGER NOT NULL,
  individual_number INTEGER NOT NULL,
  accuracy REAL,
  derivative REAL,
  residual REAL,
  PRIMARY KEY(training_path, actual_individual_number, individual_number),
  FOREIGN KEY(training_path) REFERENCES Trend(traning_path),
  FOREIGN KEY(actual_individual_number) REFERENCES Trend(individual_number)
);


--
-- Remove Trend and DiscreteValues rows on last Training with PATH deleted
--
CREATE TRIGGER trend_delete_trg
AFTER DELETE ON Training
  WHEN (SELECT COUNT(*) FROM Training WHERE path = old.path) = 0
  BEGIN
    DELETE FROM Trend WHERE training_path = old.path;
    DELETE FROM DiscreteValues WHERE training_path = old.path;
  END;