import sqlite3

from app.core.persistence.training_manager import TrainingManager
from app.core.training import Training


class DBTrainingManager(TrainingManager):

    def __init__(self, db):
        self.database = db

    def get_all_trainings(self):
        training_list = list()

        try:
            conn = self.database.get_connection()
            sql = """
            SELECT id, path, anchor, convergence_threshold, verticality_threshold, slow_down, look_ahead,
            kernel, arithmetic_step, geometric_factor, adaptative_port, corpus, tagger, max_indiv_size, state
            FROM Training
            """

            res = conn.execute(sql)
            for row in res:
                training_list.append(
                    Training(row[1], row[2], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12],
                             row[13], row[14], training_id=row[0], convergence_threshold=row[3])
                )

            return training_list

        except sqlite3.Error, e:
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def get_pending_trainings(self):
        training_list = list()

        try:
            conn = self.database.get_connection()
            sql = """
            SELECT id, path, anchor, convergence_threshold, verticality_threshold, slow_down, look_ahead,
            kernel, arithmetic_step, geometric_factor, adaptative_port, corpus, tagger, max_indiv_size, state
            FROM Training
            WHERE state="started"
            """

            res = conn.execute(sql)

            for row in res:
                training_list.append(
                    Training(row[1], row[2], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12],
                             row[13], row[14], training_id=row[0], convergence_threshold=row[3])
                )

            return training_list

        except sqlite3.Error, e:
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def exist(self, training):
        try:
            conn = self.database.get_connection()

            values = {
                    "path": training.get_path(),
                    "anchor": training.get_anchor(),
                    "convergence_threshold": training.get_convergence_threshold(),
                    "verticality_threshold": training.get_verticality_threshold(),
                    "slow_down": training.get_slow_down(),
                    "look_ahead": training.get_look_ahead(),
                    "kernel": training.get_kernel(),
                    "corpus": training.get_corpus(),
                    "tagger": training.get_tagger(),
                    "max_indiv_size": training.get_max_indiv_size()
                }

            sql = """SELECT *
                   FROM Training
                   WHERE path=:path AND anchor=:anchor AND convergence_threshold=:convergence_threshold AND
                   verticality_threshold=:verticality_threshold AND slow_down=:slow_down AND
                   look_ahead=:look_ahead AND kernel=:kernel AND
                   corpus=:corpus AND tagger=:tagger AND max_indiv_size=:max_indiv_size"""

            if training.get_arithmetic_step():
                sql += " AND arithmetic_step=:arithmetic_step "
                values["arithmetic_step"] = training.get_arithmetic_step()
            else:
                sql += " AND arithmetic_step is NULL"

            if training.get_geometric_factor():
                sql += " AND geometric_factor=:geometric_factor"
                values["geometric_factor"] = training.get_geometric_factor()
            else:
                sql += " AND geometric_factor is NULL"

            if training.get_adaptative_port():
                sql += " AND adaptative_port=:adaptative_port "
                values["adaptative_port"] = training.get_adaptative_port()
            else:
                sql += " AND adaptative_port is NULL"

            res = conn.execute(sql, values)

            count = 0
            for row in res:
                count += 1

            return count >= 1

        except sqlite3.Error, e:
            print "Error while trying to look if training exist: {}".format(e)

        finally:
            conn.close()

    def add(self, training):
        try:
            conn = self.database.get_connection()
            cur = conn.cursor()
            cur.execute(
                """INSERT INTO Training VALUES (NULL, :path, :anchor, :convergence_threshold, :verticality_threshold,
                   :slow_down, :look_ahead, :kernel, :arithmetic_step, :geometric_factor, :adaptative_port,
                   :corpus, :tagger, :max_indiv_size, :state)""",
                {
                    "path": training.get_path(),
                    "anchor": training.get_anchor(),
                    "verticality_threshold": training.get_verticality_threshold(),
                    "convergence_threshold": training.get_convergence_threshold(),
                    "slow_down": training.get_slow_down(),
                    "look_ahead": training.get_look_ahead(),
                    "kernel": training.get_kernel(),
                    "arithmetic_step": training.get_arithmetic_step(),
                    "geometric_factor": training.get_geometric_factor(),
                    "adaptative_port": training.get_adaptative_port(),
                    "corpus": training.get_corpus(),
                    "tagger": training.get_tagger(),
                    "max_indiv_size": training.get_max_indiv_size(),
                    "state": training.get_state()
                }
            )
            ok = cur.rowcount == 1
            if ok:
                conn.commit()

            return cur.lastrowid, ok

        except sqlite3.Error, e:
            conn.rollback()
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def add_list(self, training_list):
        try:
            conn = self.database.get_connection()
            tuples_list = self._training_list_to_tuples_list(training_list)
            res = conn.executemany("INSERT INTO Training VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", tuples_list)
            ok = res.rowcount == len(training_list)
            if ok:
                conn.commit()

            return ok

        except sqlite3.Error, e:
            conn.rollback()
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def update_training(self, training):
        try:
            conn = self.database.get_connection()
            cur = conn.cursor()
            cur.execute(
                """UPDATE Training
                   SET path=:path, anchor=:anchor, convergence_threshold=:convergence_threshold,
                   verticality_threshold=:verticality_threshold, slow_down=:slow_down, look_ahead=:look_ahead,
                   kernel=:kernel, arithmetic_step=:arithmetic_step, geometric_factor=:geometric_factor,
                   adaptative_port=:adaptative_port, corpus=:corpus, tagger=:tagger, max_indiv_size=:max_indiv_size,
                   state=:state
                   WHERE id=:id""",
                {
                    "path": training.get_path(),
                    "anchor": training.get_anchor(),
                    "verticality_threshold": training.get_verticality_threshold(),
                    "convergence_threshold": training.get_convergence_threshold(),
                    "slow_down": training.get_slow_down(),
                    "look_ahead": training.get_look_ahead(),
                    "kernel": training.get_kernel(),
                    "arithmetic_step": training.get_arithmetic_step(),
                    "geometric_factor": training.get_geometric_factor(),
                    "adaptative_port": training.get_adaptative_port(),
                    "corpus": training.get_corpus(),
                    "tagger": training.get_tagger(),
                    "max_indiv_size": training.get_max_indiv_size(),
                    "state": training.get_state(),
                    "id": training.get_id()
                }
            )
            ok = cur.rowcount == 1
            if ok:
                conn.commit()

            return ok

        except sqlite3.Error, e:
            conn.rollback()
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def remove(self, training):
        try:
            conn = self.database.get_connection()
            res = conn.execute("DELETE FROM Training WHERE id=?", (training.get_id(),))
            ok = res.rowcount == 1
            if ok:
                conn.commit()

            return ok

        except sqlite3.Error, e:
            conn.rollback()
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def delete_all(self):
        try:
            conn = self.database.get_connection()
            res = conn.execute("DELETE FROM Training")
            ok = res.rowcount > 1
            if ok:
                conn.commit()
            else:
                raise Exception("There are no trainings to delete.")

        except sqlite3.Error, e:
            conn.rollback()
            raise e

        finally:
            conn.close()

    @staticmethod
    def _training_list_to_tuples_list(training_list):
        return [(t.get_path(), t.get_anchor(), t.get_convergence_threshold(), t.get_verticality_threshold(),
                 t.get_slow_down(), t.get_look_ahead(), t.get_kernel(), t.get_arithmetic_step(),
                 t.get_geometric_factor(), t.get_adaptative_port(), t.get_corpus(), t.get_tagger(),
                 t.get_max_indiv_size(), t.get_state())
                 for t in training_list]





