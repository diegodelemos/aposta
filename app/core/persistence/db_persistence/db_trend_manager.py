import sqlite3

from app.core.trend import Trend
from app.core.persistence.trend_manager import TrendManager


class DBTrendManager(TrendManager):

    def __init__(self, db):
        self.database = db

    def get_trends_by_training(self, training):
        trend_list = list()

        try:
            conn = self.database.get_connection()
            res = conn.execute(
                """
                SELECT training_path, individual_number, a, b, c, slope, rmse
                FROM Trend
                WHERE training_path=? AND individual_number<=?
                """,
                (training.get_path(), training.get_state()))

            for row in res:
                training_path = row[0]
                individual_number = row[1]
                a = row[2]
                b = row[3]
                c = row[4]
                slope = row[5]
                rmse = row[6]
                discrete_values = self._get_discrete_values(conn, training_path, individual_number)
                trend_list.append(Trend(individual_number, a, b, c, slope, rmse, discrete_values))

            return trend_list

        except sqlite3.Error, e:
            print "{}".format(e.args[0])

        finally:
            conn.close()

    def add_list(self, training):
        try:
            conn = self.database.get_connection()
            last_trend = None
            if self._training_data_exist(conn, training.get_path()):
                last_trend = self._get_last_trend_number(conn, training)
                if last_trend <= training.get_trends()[-1].get_individual_number():
                    return True

            trend_table,  discrete_values_table = self._training_to_tuples_list(training,
                                                                                last_existing_trend=last_trend)
            res_trends = conn.executemany("INSERT INTO Trend VALUES (?,?,?,?,?,?,?)", trend_table)

            res_discrete_values = conn.executemany("INSERT INTO DiscreteValues VALUES (?,?,?,?,?,?)",
                                                   discrete_values_table)

            if res_trends.rowcount == len(trend_table) and res_discrete_values.rowcount == len(discrete_values_table):
                conn.commit()
                return True
            else:
                conn.rollback()
                return False

        except sqlite3.Error, e:
            conn.rollback()
            print "{}".format(e.args[0])

        finally:
            conn.close()

    @staticmethod
    def _training_to_tuples_list(training, last_existing_trend=None):

        trend_table = list()
        discrete_values_table = list()
        trends = training.get_trends()

        if last_existing_trend:
            trends = [trend for trend in trends if trend.get_individual_number() > last_existing_trend]

        for trend in trends:
            trend_table.append((training.get_path(), trend.get_individual_number(), trend.get_a(), trend.get_b(),
                                trend.get_asymptote(), trend.get_slope(), trend.get_rmse()))

            dv = trend.get_discrete_values()
            discrete_values_table.extend([(training.get_path(), trend.get_individual_number(), k,
                                           dv[k][Trend.discrete_value.Value], dv[k][Trend.discrete_value.Derivative],
                                           dv[k][Trend.discrete_value.Residual])
                                           for k in dv.keys()])

        return trend_table, discrete_values_table

    @staticmethod
    def _get_discrete_values(conn, t_path, actual_individual):
        try:

            res = conn.execute(
                """SELECT individual_number, accuracy, derivative, residual
                   FROM DiscreteValues
                   WHERE training_path=:t_path and actual_individual_number=:actual_individual""",
                {
                    "t_path": t_path,
                    "actual_individual": actual_individual
                }
            )
            trend_discrete_values = dict()
            for row in res:
                if row[0] <= actual_individual:
                    trend_discrete_values[row[0]] = {
                        Trend.discrete_value.Value: float(row[1]),
                        Trend.discrete_value.Derivative: float(row[2]),
                        Trend.discrete_value.Residual: float(row[3])
                    }
                else:
                    trend_discrete_values[row[0]] = {
                        Trend.discrete_value.Value: None,
                        Trend.discrete_value.Derivative: None,
                        Trend.discrete_value.Residual: float(row[3])
                    }

            return trend_discrete_values

        except sqlite3.Error, e:
            print "Error retrieving discrete values from DB: {}".format(e.args[0])

    @staticmethod
    def _training_data_exist(conn, path):
        try:
            cur = conn.cursor()
            cur.execute("SELECT * FROM Trend WHERE training_path=?", (path, ))
            return len(cur.fetchall()) > 0

        except sqlite3.Error, e:
            print "Error checking if trend data already exists: {}".format(e.args[0])

    @staticmethod
    def _get_last_trend_number(conn, training):
        try:
            cur = conn.cursor()
            cur.execute("SELECT max(individual_number) FROM Trend WHERE training_path=?", (training.get_path(), ))
            return cur.fetchone()[0]

        except sqlite3.Error, e:
            print "Error retrieving the last trend for an existing training: {}".format(e.args[0])

