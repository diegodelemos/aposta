import os
from app import config
from app.core.training import Training
from app.core.persistence.training_provider import TrainingProvider


class DirectoryTreeTrainingProvider(TrainingProvider):
    """
    Concrete training provider. This implementation is created for
    reading the trainings from a directory tree.
    """

    def __init__(self, path):
        if os.path.isdir(path):
            self.path = path
        else:
            raise Exception("Check the configuration file. Missing or incorrect APOSTA directories.".format(path))

    def get_all_trainings(self):
        """
        Gets the available trainings in the program directory. It takes into account
        whether the trainings has a available tagger and corpus. Once it gets the
        training it creates the Training object.
        """
        training_list = list()

        for dir_path, dir_names, file_names in os.walk(self.path):
            for dir_name in dir_names:
                if dir_name in config.available_corpus:
                    for tagger in os.listdir(os.path.join(dir_path, dir_name)):
                        if tagger in config.available_taggers:
                            path = os.path.join(dir_path, dir_name, tagger)
                            trainings = self._log_file_reader(path)
                            training_list.extend(self._create_training(path, trainings))

        return training_list

    def get_pending_trainings(self):
        pending_trainings = list()
        for training in self.get_all_trainings():
            if training.get_state() == "started":
                pending_trainings.append(training)

        return pending_trainings

    def is_ended_training(self, training):
        """

        :param training: training to be checked
        :return: tuple (State, Is_ended)
        """
        base_path = os.path.join(self.path, training.get_path())
        if not self._log_file_exists(base_path):
            return None, False

        trainings = self._log_file_reader(base_path)
        for t_id, state in trainings.items():
            t_id = int(t_id)
            if t_id == training.get_id():
                try:
                    # there are two possible ways of finnishing for a training
                    if state == "No":
                        return "No", True
                    else:
                        state = int(state)
                        return state, True

                except ValueError:
                    return state, False

        return None, False

    def _create_training(self, path, trainings):
        """
        Function that parses the training path in order to create a correct Training object
        path example:
        '/home/diego/TFG/code/aposta_repo/stopping_criterion_dir/training/|fixed/1e-4_2_10/10000_geometric_1.005/anCora/TreeTagger'
        [  0       1              2              3       4      ]
              [ 0  1  2][  0      1       2  ]
        fixed/1e-4_2_10/10000_geometric_1.005/anCora/TreeTagger'
        ancla/umbral_verticalidad_freno_acarreo/kernel_tipopaso_paso/corpus/tagger
        """
        ret = list()
        parameters = path.split("/")[-5:]

        anchor = parameters[0]  # TODO test if its within the available ancors
        vert_slowdown_lookahead = parameters[1].split("_")
        verticality_threshold = float(vert_slowdown_lookahead[0])
        slow_down = float(vert_slowdown_lookahead[1])
        look_ahead = float(vert_slowdown_lookahead[2])
        kernel_stepfuncion_step = parameters[2].split("_")
        kernel = int(kernel_stepfuncion_step[0])
        step_function = kernel_stepfuncion_step[1]  # TODO check if its an available step
        step = float(kernel_stepfuncion_step[2])
        corpus = parameters[3]
        tagger = parameters[4]

        # since there can be more than one run, we create a training
        # for each one.
        training_path = path.replace(self.path, '')
        for t_id in trainings.keys():
            if step_function == "geometric":
                geometric_factor = step
            else:
                geometric_factor = None

            if step_function == "arithmetic" or step_function == "uniform":
                arithmetic_step = int(step)
            else:
                arithmetic_step = None

            adaptative_port = None
            max_indiv_size = 800000
            ret.append(
                Training(training_path, anchor, verticality_threshold, slow_down, look_ahead, kernel, arithmetic_step,
                         geometric_factor, adaptative_port, corpus, tagger, max_indiv_size, trainings[t_id],
                         training_id=t_id)
            )

        return ret

    @staticmethod
    def _log_file_exists(training_path):
        log_file = os.path.join(training_path, ".log")
        exists = os.path.isfile(log_file)
        return exists

    @staticmethod
    def _log_file_reader(path):
        trainings = dict()
        with open(os.path.join(path, ".log")) as f:
            for line in f.readlines():
                line = line.strip()
                if line:
                    line = line.split("->")
                    result = line[1].split()
                    if result[2] == "done:":
                        trainings[result[1]] = result[3]
                    else:
                        trainings[result[1]] = result[2]

        return trainings
