import os
import csv
from app import config
from app.core.trend import Trend
from app.core.persistence.trend_provider import TrendProvider


class DirectoryTreeTrendProvider(TrendProvider):

    def __init__(self, path):
        if os.path.isdir(path):
            self.path = path
        else:
            raise Exception("Check the configuration file. Missing or incorrect APOSTA directories.".format(path))

    def get_trends_by_training(self, training):
        path = os.path.join(self.path, training.get_path())

        trend_list = list()
        for individual_number in self._observations_reader(path):
            trend_list.append(
                self._read_from_file(
                    os.path.join(
                        path,
                        individual_number,
                        "trend-{}.txt".format(individual_number)
                    ),
                    individual_number
                )
            )
        return trend_list

    @staticmethod
    def _observations_reader(path):
        individual_list = []
        observations_file_name = "observations.txt"
        with open(os.path.join(path, observations_file_name)) as observations_file:
            reader = csv.reader(observations_file, delimiter='=')
            # skip N first trends because they do not have any info.
            for i in range(config.starting_individual-1):
                next(reader)

            for row in reader:
                individual_list.append(row[0].strip())

        return individual_list

    @staticmethod
    def _read_from_file(file_name, individual_number):
        """
        Method that reads the trend info from file and
        creates an instance of trend class
        """
        a = 0.0                # float
        b = 0.0                # float
        c = 0.0                # float
        slope = 0.0            # float
        rmse = 0.0             # float
        trend_discrete_values = dict() # individual{ value, derivative, residual}
        with open(file_name) as trend_file:
            lines = trend_file.read().split("\n")
            a = float(lines[1].split("=")[1].strip())
            b = float(lines[2].split("=")[1].strip())
            c = float(lines[3].split("=")[1].strip())
            slope = float(lines[4].split("=")[1].strip())
            rmse_line = lines[5].split("=")
            if rmse_line[0].strip() == "RMSE":
                rmse_value = lines[5].split("=")[1].strip()
                rmse = None if rmse_value == "NaN" else float(rmse_value)
            else:
                # if RMSE is not in line 5 it will be in line 6
                rmse_value = lines[6].split("=")[1].strip()
                rmse = None if rmse_value == "NaN" else float(rmse_value)

            discrete_values_lines = lines[7:]
            for discrete_values_line in discrete_values_lines:
                elements = discrete_values_line.split()
                if elements and elements[0].strip() != "Indiv.":
                    current_individual = float(elements[0])
                    if current_individual <= float(individual_number):
                        trend_discrete_values[current_individual] = {
                            Trend.discrete_value.Value: float(elements[1]),
                            Trend.discrete_value.Derivative: float(elements[2]),
                            Trend.discrete_value.Residual: float(elements[3])
                        }
                    else:
                        trend_discrete_values[current_individual] = {
                            Trend.discrete_value.Value: None,
                            Trend.discrete_value.Derivative: None,
                            Trend.discrete_value.Residual: float(elements[1])
                        }

        return Trend(int(individual_number), a, b, c, slope, rmse, trend_discrete_values)

