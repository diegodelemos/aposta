from app.core.persistence.training_provider import TrainingProvider


class TrainingManager(TrainingProvider):

    def get_pending_trainings(self):
        pass

    def get_all_trainings(self):
        pass

    def exist(self, training):
        raise NotImplementedError

    def add(self, training):
        raise NotImplementedError

    def remove(self, training):
        raise NotImplementedError
