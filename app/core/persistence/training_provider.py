class TrainingProvider:

    def get_all_trainings(self):
        raise NotImplementedError

    def get_pending_trainings(self):
        raise NotImplementedError
