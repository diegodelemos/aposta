from app.core.trend import Trend


class PlotDataGenerator:

    @staticmethod
    def get_trends_plot_data(training, selected_trends):
        trend_functions = list()
        for trend in selected_trends:
            trend_functions.append(
                (
                    trend.get_individual_number(),
                    trend.get_function(training.get_training_range())
                )
            )
        return trend_functions

    @staticmethod
    def get_training_derivatives_plot_data(training):
        values, derivatives = training.get_trends()[-1].get_discrete_value(Trend.discrete_value.Derivative)
        return values, derivatives

    @staticmethod
    def get_training_point_cloud_plot_data(training):
        return training.get_trends()[-1].get_discrete_value(Trend.discrete_value.Value)

    @staticmethod
    def get_residuals_inf_plot_data(training):
        residuals_inf = (list(), list())
        trends = training.get_trends()
        last_trend_number = float(trends[-1].get_individual_number())
        for trend in trends:
            last = trend.get_discrete_value(Trend.discrete_value.Residual)
            if float(last[0][-1]) > last_trend_number:
                residuals_inf[0].append(trend.get_individual_number())
                residuals_inf[1].append(last[1][-1])

        return residuals_inf

    @staticmethod
    def get_residuals_sum_plot_data(training):
        residuals_sum = (list(), list())
        trends = training.get_trends()
        last_trend_number = float(trends[-1].get_individual_number())
        for trend in trends:
            last = trend.get_discrete_value(Trend.discrete_value.Residual)
            residuals_sum[0].append(trend.get_individual_number())
            if float(last[0][-1]) > last_trend_number:
                residuals_sum[1].append(sum(last[1][:-1]))
            else:
                residuals_sum[1].append(sum(last[1]))

        return residuals_sum

    @staticmethod
    def get_residuals_diff_same_individual_plot_data(training):
        residuals_diff = (list(), list())
        trends = training.get_trends()
        last_trend_number = float(trends[-1].get_individual_number())

        for trend in trends:
            last = trend.get_discrete_value(Trend.discrete_value.Residual)

            if float(last[0][-1]) < last_trend_number:
                residuals_diff[0].append(trend.get_individual_number())
                residuals_diff[1].append(last[1][-1] - last[1][-2])

        return residuals_diff

    @staticmethod
    def get_residuals_diff_prev_individual_plot_data(training):
        residuals_diff = (list(), list())
        trends = training.get_trends()
        last_trend_number = float(trends[-1].get_individual_number())
        previous_trend = trends[0]
        trends = trends[1:]
        for actual_trend in trends:
            prev_values = previous_trend.get_discrete_value(Trend.discrete_value.Residual)
            actual_values = actual_trend.get_discrete_value(Trend.discrete_value.Residual)
            if float(actual_values[0][-1]) < last_trend_number:
                residuals_diff[0].append(actual_trend.get_individual_number())
                residuals_diff[1].append(prev_values[1][-1] - actual_values[1][-1])

            previous_trend = actual_trend

        return residuals_diff

    @staticmethod
    def get_asymptote_plot_data(training):
        asymptotes = (list(), list())
        trends = training.get_trends()
        for trend in trends:
            asymptotes[0].append(trend.get_individual_number())
            asymptotes[1].append(trend.get_asymptote())

        return asymptotes
