from app import config


class Training:
    """
    Class that represents a training not only physically (path for disk retrieval), but also
    symbollically to have it indentified by its parameters
    ancor, Umbral de verticalidad, freno, acarreo, kernel, tipo de paso, corpus, tagger.
    """
    def __init__(self, path, anchor, verticality_threshold, slow_down, look_ahead, kernel, arithmetic_step,
                 geometric_factor, adaptative_port, corpus, tagger, max_indiv_size, state, trends=None, training_id=0,
                 convergence_threshold=0.0):

        self._path = path
        self._training_id = training_id
        self._anchor = anchor
        self._convergence_threshold = convergence_threshold
        self._verticality_threshold = verticality_threshold
        self._slow_down = slow_down
        self._look_ahead = look_ahead
        self._kernel = kernel
        self._arithmetic_step = arithmetic_step
        self._geometric_factor = geometric_factor
        self._adaptative_port = adaptative_port
        self._corpus = corpus
        self._tagger = tagger
        self._state = state
        self._max_indiv_size = max_indiv_size
        self._trends = trends or []
        self._trend_provider = None

    def get_path(self):
        if self._path is None or self._path == "":
            self._path = self._build_path()

        return self._path

    def get_id(self):
        return self._training_id

    def set_id(self, training_id):
        self._training_id = training_id

    def get_anchor(self):
        return self._anchor

    def get_convergence_threshold(self):
        return self._convergence_threshold

    def get_verticality_threshold(self):
        return self._verticality_threshold

    def get_slow_down(self):
        return self._slow_down

    def get_look_ahead(self):
        return self._look_ahead

    def get_kernel(self):
        return self._kernel

    def get_arithmetic_step(self):
        return self._arithmetic_step

    def get_geometric_factor(self):
        return self._geometric_factor

    def get_adaptative_port(self):
        return self._adaptative_port

    def get_corpus(self):
        return self._corpus

    def get_tagger(self):
        return self._tagger

    def get_state(self):
        return self._state

    def set_state(self, state):
        self._state = state

    def get_max_indiv_size(self):
        return self._max_indiv_size

    def get_trends(self):
        # Abstract TrendProvider
        self._trends = self._trend_provider.get_trends_by_training(self)
        # From now on whenever self.get_trends() is called, self.get_trends_post() will return its value.
        # It could be a
        self.get_trends = self.get_trends_post
        return self._trends

    def get_trends_post(self):
        return self._trends

    def get_training_range(self):
        try:
            end = int(self.get_state())
        except ValueError:
            end = self._trends[-1].get_individual_number()
        return xrange(
            int(self._trends[0].get_individual_number()),
            end,
            config.function_increment
        )

    def set_trend_provider(self, provider):
        self._trend_provider = provider

    def _build_path(self):
        """
        Returns a reconstruction of the training path
        path example:
        '/home/diego/TFG/code/aposta_repo/stopping_criterion_dir/no-anchor/1e-4_2_10/10000_geometric_1.005/anCora/TreeTagger'
        """
        import os
        try:
            fmt_slow_down = self.get_slow_down()
            if fmt_slow_down == int(fmt_slow_down):
                fmt_slow_down = int(fmt_slow_down)

        except ValueError:
            fmt_slow_down = self.get_slow_down()

        try:
            fmt_look_ahead = self.get_look_ahead()
            if fmt_look_ahead == int(fmt_look_ahead):
                fmt_look_ahead = int(fmt_look_ahead)

        except ValueError:
            fmt_look_ahead = self.get_look_ahead()

        fmt_verticality_threshold = str(self.get_verticality_threshold())
        if 'e-' in fmt_verticality_threshold:
            exp_pos = fmt_verticality_threshold.index('e-') + 2
            if fmt_verticality_threshold[exp_pos] == '0':
                fmt_verticality_threshold = fmt_verticality_threshold[:exp_pos] + fmt_verticality_threshold[exp_pos+1:]
        else:
            fmt_verticality_threshold = self.get_verticality_threshold()

        # Adaptative step with geometric until level of work: .../KERNEL_geometric_FACTOR_port_PORT/...
        if self.get_geometric_factor() and self.get_adaptative_port():
            step_function = "geometric"
            port = "port"
            path = os.path.join(str(self.get_anchor()),
                                "{0}_{1}_{2}".format(fmt_verticality_threshold,
                                                     fmt_slow_down,
                                                     fmt_look_ahead
                                                     ),
                                "{0}_{1}_{2}_{3}_{4}".format(self.get_kernel(),
                                                             step_function,
                                                             self.get_geometric_factor(),
                                                             port,
                                                             self.get_adaptative_port()
                                                             ),
                                self.get_corpus(),
                                self.get_tagger()
                                )
        # Adaptative step with arithmetic until level of work: .../KERNEL_arithmetic_STEP_port_PORT/...
        elif self.get_arithmetic_step() and self.get_adaptative_port():
            step_function = "arithmetic"
            port = "port"
            path = os.path.join(str(self.get_anchor()),
                                "{0}_{1}_{2}".format(fmt_verticality_threshold,
                                                     fmt_slow_down,
                                                     fmt_look_ahead
                                                     ),
                                "{0}_{1}_{2}_{3}_{4}".format(self.get_kernel(),
                                                             step_function,
                                                             self.get_arithmetic_step(),
                                                             port,
                                                             self.get_adaptative_port()
                                                             ),
                                self.get_corpus(),
                                self.get_tagger()
                                )
        # Arithmetic step until level of work and then geometric: .../KERNEL_arithmetic_STEP_geometric_FACTOR/...
        elif self.get_arithmetic_step() and self.get_geometric_factor():
            arithmetic = "arithmetic"
            geometric = "geometric"
            path = os.path.join(str(self.get_anchor()),
                                "{0}_{1}_{2}".format(fmt_verticality_threshold,
                                                     fmt_slow_down,
                                                     fmt_look_ahead
                                                     ),
                                "{0}_{1}_{2}_{3}_{4}".format(self.get_kernel(),
                                                             arithmetic,
                                                             self.get_arithmetic_step(),
                                                             geometric,
                                                             self.get_geometric_factor()
                                                             ),
                                self.get_corpus(),
                                self.get_tagger()
                                )
        # Just arithmetic step: .../KERNEL_arithmetic_STEP/...
        elif self.get_arithmetic_step():
            step_function = "arithmetic"
            path = os.path.join(str(self.get_anchor()),
                                "{0}_{1}_{2}".format(fmt_verticality_threshold,
                                                     fmt_slow_down,
                                                     fmt_look_ahead
                                                     ),
                                "{0}_{1}_{2}".format(self.get_kernel(),
                                                     step_function,
                                                     self.get_arithmetic_step()
                                                     ),
                                self.get_corpus(),
                                self.get_tagger()
                                )
        # Just geometric factor: .../KERNEL_geometric_FACTOR/...
        elif self.get_geometric_factor():
            step_function = "geometric"
            path = os.path.join(str(self.get_anchor()),
                                "{0}_{1}_{2}".format(fmt_verticality_threshold,
                                                     fmt_slow_down,
                                                     fmt_look_ahead
                                                     ),
                                "{0}_{1}_{2}".format(self.get_kernel(),
                                                     step_function,
                                                     self.get_geometric_factor()
                                                     ),
                                self.get_corpus(),
                                self.get_tagger()
                                )

        return path

