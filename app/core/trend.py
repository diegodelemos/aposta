from app.core.enum import Enum


class Trend:
    discrete_value = Enum(["Residual", "Value", "Derivative"])

    def __init__(self, individual_number, a, b, c, slope,
                 rmse, discrete_values):

        self._individual_number = individual_number
        self._a = a
        self._b = b
        self._c = c
        self._slope = slope
        self._rmse = rmse
        self._discrete_values = discrete_values

    def get_a(self):
        return self._a

    def get_b(self):
        return self._b

    def get_asymptote(self):
        return self._c

    def get_slope(self):
        return self._slope

    def get_rmse(self):
        return self._rmse

    def get_discrete_values(self):
        return self._discrete_values

    def get_function_as_str(self):
        return "{a}*x**{b}+{c}".format(a=self.get_a(), b=self.get_b(), c=self.get_asymptote())

    def get_function(self, range_):
        return [self._a * (x ** self._b) + self._c
                for x in range_]

    def get_individual_number(self):
        return self._individual_number

    def get_discrete_value(self, parameter):
        """
        Parameter represent which of the values we want to get
        ["value" | "derivative" | "residual"]
        Returns a list of tubles which contains the individual
        number and its parameter value.
        :rtype: tuple(individual_list, discrete_values_list)
        """
        discrete_values = list()
        discrete_parameters = list()
        individuals = self._discrete_values.keys()
        individuals.sort()
        for individual in individuals:
            discrete_values.append(individual)
            discrete_parameters.append(self._discrete_values[individual][parameter])
                    
        return discrete_values, discrete_parameters
