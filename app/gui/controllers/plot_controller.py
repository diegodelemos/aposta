from app.core.core_facade import CoreFacade


class PlotController(object):

    def __init__(self, app, model, training):
        self.app = app
        self.model = model
        self.training = training

    def get_selected_trends_plot_data(self):
        return CoreFacade.get_trends_plot_data(self.training, self.model.get_checked_trends())

    def get_trend_derivatives_data(self):
        return CoreFacade.get_training_derivatives_plot_data(self.training)

    def get_trend_point_cloud_data(self):
        return CoreFacade.get_training_point_cloud_plot_data(self.training)

    def get_residuals_inf(self):
        return CoreFacade.get_residuals_inf_plot_data(self.training)

    def get_residuals_sum(self):
        return CoreFacade.get_residuals_sum_plot_data(self.training)

    def get_residuals_diff_same_individual(self):
        return CoreFacade.get_residuals_diff_same_individual_plot_data(self.training)

    def get_residuals_diff_prev_individual(self):
        return CoreFacade.get_residuals_diff_prev_individual_plot_data(self.training)

    def get_asymptote_plot_data(self):
        return CoreFacade.get_asymptote_plot_data(self.training)

    def save_data_to_text_file(self, filename, discrete):
        """

        :param filename: path where the file will be saved
        :param discrete: dictionary composed by; graph type (as key) and a tuple composed by two list, x and y values
        """
        with open(filename, "w") as plot_file:
            trends = self.model.get_checked_trends()
            if trends or discrete:
                plot_file.write("Training data:\n")
                plot_file.write("Training id {}\n".format(self.training.get_id()))
                plot_file.write("Anchor {}\n".format(self.training.get_anchor()))
                plot_file.write("Corpus {}\n".format(self.training.get_corpus()))
                plot_file.write("Tagger {}\n".format(self.training.get_tagger()))
                plot_file.write("Kernel {}\n".format(self.training.get_kernel()))
                if self.training.get_arithmetic_step():
                    plot_file.write("Arithmetic step: {}\n".format(self.training.get_arithmetic_step()))

                if self.training.get_geometric_factor():
                    plot_file.write("Geometric factor: {}\n".format(self.training.get_geometric_factor()))

                if self.training.get_adaptative_port():
                    plot_file.write("Adaptative port: {}\n".format(self.training.get_adaptative_port()))

            if trends:
                plot_file.write("----------------------------------------------------\n")
                plot_file.write("Trend list:\n".format())
                for trend in self.model.get_checked_trends():
                    plot_file.write("{f_level} = {function}\n".format(f_level=trend.get_individual_number(),
                                                                      function=trend.get_function_as_str()))

            if discrete:
                for value_name in discrete.keys():
                    plot_file.write("----------------------------------------------------\n")
                    plot_file.write("{} table:\n".format(value_name))
                    plot_file.write("\n")
                    plot_file.write("{col0_title}\t{col1_title}\n".format(col0_title="level", col1_title=value_name))
                    level, value = discrete[value_name]
                    plot_file.writelines(
                        ["{level}\t{value}\n".format(level=level, value=value) for level, value in zip(level, value)]
                    )

    def delete_plot_manager(self, view):
        self.app.delete_plot_view(view)
