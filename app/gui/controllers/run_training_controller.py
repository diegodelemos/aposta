from app.core.training import Training
from app.core.core_facade import CoreFacade


class RunTrainingController:

    def __init__(self, app):
        self.app = app

    def run_test(self, anchor, nu, varsigma, lambda_, kernel_length, arithmetic_step, geometric_factor, adaptative_port,
                 convergence_threshold, corpus, tagger, max_indiv_size, num_process):

        if not arithmetic_step:
            arithmetic_step = None

        if not geometric_factor:
            geometric_factor = None

        if not adaptative_port:
            adaptative_port = None

        training = Training("", anchor, nu, varsigma, lambda_, kernel_length, arithmetic_step, geometric_factor,
                            adaptative_port, corpus, tagger, max_indiv_size, "started",
                            convergence_threshold=convergence_threshold)

        try:
            CoreFacade.run_training(training, num_process)
            return True, training
        except Exception, e:
            return False, str(e)

    def show_training_list_view(self, parent):
        self.app.show_training_list_view(parent)
