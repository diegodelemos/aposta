try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtCore
except ImportError:
    from PySide import QtCore

from app.core.core_facade import CoreFacade


class TrainingListController(object):
    
    def __init__(self, app, model):
        self.app = app
        self.model = model

    def consult_training(self, training):
        self.app.show_plot_view(training)

    def delete_training(self, training):
        ok = CoreFacade.delete_training(training)
        if ok:
            source_model = self.model.sourceModel()
            source_model.delete_training_by_id(training.get_id())

        return ok

    def dump_dirs_to_db(self):
        try:
            CoreFacade.dump_dir_tree_to_db()
            self.model.sourceModel().set_trainings(CoreFacade.get_available_trainings())
            return True, "Trainings successfully copied."
        except Exception, e:
            return False, str(e)

    def delete_all(self):
        try:
            CoreFacade.delete_all()
            self.model.sourceModel().delete_all()
            return True, "Trainings successfully deleted."
        except Exception, e:
            return False, str(e)

    def refresh_trainings(self):
        try:
            CoreFacade.refresh_started_trainings()
            self.model.sourceModel().delete_all()
            self.model.sourceModel().set_trainings(CoreFacade.get_available_trainings())
            return True, ""
        except Exception, e:
            return False, str(e)

    def rerun_training(self, training, num_process):
        CoreFacade.rerun_training(training, num_process)




