try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtCore, QtGui
except ImportError:
    from PySide import QtCore, QtGui

from app.gui.views.run_training_view import RunTrainingView
from app.gui.controllers.run_training_controller import RunTrainingController

from app.gui.views.training_list_view import TrainingListView
from app.gui.controllers.training_list_controller import TrainingListController
from app.gui.models.training_table_model import TrainingTableModel

from app.gui.views.plot_view import PlotView
from app.gui.controllers.plot_controller import PlotController
from app.gui.models.trend_list_model import TrendListModel


class gAposta(QtGui.QApplication):

    def __init__(self, argv):
        # creating Qt application
        QtGui.QApplication.__init__(self, argv)
        self.setApplicationName('gAPOSTA')
        self.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))
        self.training_list_view = None
        self.plot_views = list()

    def start(self):
        # creating bindings between main ctrl and main view
        self.main_ctrl = RunTrainingController(self)
        self.main_view = RunTrainingView(self.main_ctrl)
        self.main_view.show()

    def show_training_list_view(self, parent=None):
        if self.training_list_view:
            print "entro por el existente"
            self.training_list_view.show()
        else:
            from app.gui.models.training_list_proxy_model import TrainingListProxyModel
            from app.core.core_facade import CoreFacade
            training_table_model = TrainingTableModel()
            training_table_model.set_trainings(CoreFacade.get_available_trainings())
            training_proxy_model = TrainingListProxyModel()
            training_proxy_model.setSourceModel(training_table_model)
            training_list_controller = TrainingListController(self, training_proxy_model)
            self.training_list_view = TrainingListView(
                training_proxy_model,
                training_list_controller,
                parent
            )
            self.training_list_view.show()

    def show_plot_view(self, training, parent=None):
        model = TrendListModel(training.get_trends())
        controller = PlotController(self, model, training)
        view = PlotView(
            model,
            controller,
            training,
            parent
        )
        self.plot_views.append(view)
        view.show()

    def delete_plot_view(self, view):
        del(self.plot_views[self.plot_views.index(view)])
