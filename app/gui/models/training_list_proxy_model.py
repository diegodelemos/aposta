try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtCore, QtGui

except ImportError:
    from PySide import QtCore, QtGui


class TrainingListProxyModel(QtGui.QSortFilterProxyModel):

    def __init__(self, parent=None):
        super(TrainingListProxyModel, self).__init__(parent)
        self.filter_id = ''
        self.filter_corpus = ''
        self.filter_tagger = ''
        self.filter_anchor = ''
        self.filter_kernel = ''
        self.filter_convergence_threshold = ''
        self.filter_verticality_threshold = ''
        self.filter_slow_down = ''
        self.filter_lookahead = ''
        self.filter_arithmetic_step = ''
        self.filter_geometric_factor = ''
        self.filter_adaptive_PORT = ''
        self.filter_max_indiv_size = ''
        self.filter_state = ''
        self.filterFunctions = {}

    def set_filter_id(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_id = text
        self.invalidateFilter()

    def set_filter_corpus(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_corpus = text
        self.invalidateFilter()

    def set_filter_tagger(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_tagger = text
        self.invalidateFilter()

    def set_filter_anchor(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_anchor= text
        self.invalidateFilter()

    def set_filter_kernel(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_kernel = text
        self.invalidateFilter()

    def set_filter_convergence_threshold(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_convergence_threshold = text
        self.invalidateFilter()

    def set_filter_verticality_threshold(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_verticality_threshold = text
        self.invalidateFilter()

    def set_filter_slow_down(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_slow_down = text
        self.invalidateFilter()

    def set_filter_lookahead(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_lookahead = text
        self.invalidateFilter()

    def set_filter_arithmetic_step(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_arithmetic_step = text
        self.invalidateFilter()

    def set_filter_geometric_factor(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_geometric_factor = text
        self.invalidateFilter()

    def set_filter_adaptive_port(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_adaptive_PORT = text
        self.invalidateFilter()

    def set_filter_max_indiv_size(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_max_indiv_size = text
        self.invalidateFilter()

    def set_filter_state(self, text):
        """
        text : string
            The string to be used for pattern matching.
        """
        self.filter_state = text
        self.invalidateFilter()

    def filterAcceptsRow(self, row_num, parent):
        """
        Reimplemented from base class to allow the use
        of custom filtering.
        """
        model = self.sourceModel()
        # The source model should have a method called row()
        # which returns the table row as a python list.
        ok = True
        training = model.row(row_num)
        if self.filter_id:
            ok &= self.filter_id.lower() in str(training.get_id()).lower()

        if self.filter_corpus:
            ok &= self.filter_corpus.lower() in training.get_corpus().lower()

        if self.filter_tagger:
            ok &= self.filter_tagger.lower() in training.get_tagger().lower()

        if self.filter_anchor:
            ok &= self.filter_anchor.lower() in training.get_anchor().lower()

        if self.filter_kernel:
            ok &= self.filter_kernel.lower() in str(training.get_kernel()).lower()

        if self.filter_convergence_threshold:
            ok &= self.filter_convergence_threshold.lower() in str(training.get_convergence_threshold()).lower()

        if self.filter_verticality_threshold:
            ok &= self.filter_verticality_threshold.lower() in str(training.get_verticality_threshold()).lower()

        if self.filter_slow_down:
            ok &= self.filter_slow_down.lower() in str(training.get_slow_down()).lower()

        if self.filter_lookahead:
            ok &= self.filter_lookahead.lower() in str(training.get_look_ahead()).lower()

        if self.filter_arithmetic_step:
            ok &= self.filter_arithmetic_step.lower() in str(training.get_arithmetic_step()).lower()

        if self.filter_geometric_factor:
            ok &= self.filter_geometric_factor.lower() in str(training.get_geometric_factor()).lower()

        if self.filter_adaptive_PORT:
            ok &= self.filter_adaptive_PORT.lower() in str(training.get_adaptative_port()).lower()

        if self.filter_max_indiv_size:
            ok &= self.filter_max_indiv_size.lower() in str(training.get_max_indiv_size()).lower()

        if self.filter_state:
            ok &= self.filter_state.lower() in str(training.get_state()).lower()

        return ok
