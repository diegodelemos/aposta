try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtCore, QtGui

except ImportError:
    from PySide import QtCore, QtGui


class TrainingTableModel(QtCore.QAbstractTableModel):

    def __init__(self, trainings=None, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._trainings = trainings or []
        nu = u"\u03BD"
        varsigma = u"\u03C3"
        lambda_ = u"\u03BB"
        self._header_list = ["ID", "Corpus", "Tagger", "Anchor", "Kernel", "Convergence Threshold",
                             nu, varsigma, lambda_, "Step", "Factor",
                             "PORT", "Max. individual size", "State"]

        # Corpus, Tagger, Anchor, Kernel, Verticality Threshold, Slow Down, Look Ahead, Arithmetic Step,
        # Geometric Factor, Adaptive PORT, Max. Individual Size y State
        (self._ID, self._CORPUS, self._TAGGER, self._ANCHOR, self._KERNEL,
         self._CONVERGENCE_THRESHOLD, self._VERTICALITY_THRESHOLD, self._SLOW_DOWN, self._LOOK_AHEAD,
         self._ARITHMETIC_STEP, self._GEOMETRIC_FACTOR, self._ADAPTIVE_PORT, self._MAX_INDIV_SIZE,
         self._STATE) = range(len(self._header_list))

    def set_trainings(self, trainings):
        self.delete_all()
        self.beginInsertRows(QtCore.QModelIndex(), 0, len(trainings)-1)
        self._trainings = trainings
        self.endInsertRows()

    def delete_all(self):
        self.beginRemoveRows(QtCore.QModelIndex(), 0, len(self._trainings)-1)
        self._trainings = list()
        self.endRemoveRows()

    def delete_training_by_id(self, training_id):
        for pos, training in enumerate(self._trainings):
            if training.get_id() == training_id:
                self.beginRemoveRows(QtCore.QModelIndex(), pos, pos)
                del(self._trainings[pos])
                self.endRemoveRows()

    def get_training_by_id(self, training_id):
        for training in self._trainings:
            if training.get_id() == training_id:
                return training

        return None

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self._header_list[section]

    def rowCount(self, parent=None):
        return len(self._trainings)

    def columnCount(self, parent=None):
        return len(self._header_list)

    def row(self, row_num):
        return self._trainings[row_num]

    def data(self, index, role=QtCore.Qt.DisplayRole):

        column = index.column()
        training = self._trainings[index.row()]
        if role == QtCore.Qt.UserRole:
            return training

        if role == QtCore.Qt.BackgroundColorRole:
            if training.get_state() == "started":
                return QtGui.QBrush(QtGui.QColor(243, 245, 183))
            elif training.get_state() == "No":
                return QtGui.QBrush(QtGui.QColor(245, 183, 190))
            else:
                return QtGui.QBrush(QtGui.QColor(214, 249, 203))

        if role == QtCore.Qt.DisplayRole:
            if column == self._ID:
                return training.get_id()
            
            elif column == self._ANCHOR:
                return training.get_anchor()

            elif column == self._CONVERGENCE_THRESHOLD:
                return str(training.get_convergence_threshold())
            
            elif column == self._VERTICALITY_THRESHOLD:
                return str(training.get_verticality_threshold())
            
            elif column == self._SLOW_DOWN:
                return str(training.get_slow_down())
            
            elif column == self._LOOK_AHEAD:
                return str(training.get_look_ahead())
            
            elif column == self._KERNEL:
                return training.get_kernel()

            elif column == self._ARITHMETIC_STEP:
                return str(training.get_arithmetic_step()) if training.get_arithmetic_step() else ""

            elif column == self._GEOMETRIC_FACTOR:
                return str(training.get_geometric_factor()) if training.get_geometric_factor() else ""

            elif column == self._ADAPTIVE_PORT:
                return str(training.get_adaptative_port()) if training.get_adaptative_port() else ""

            elif column == self._CORPUS:
                return training.get_corpus()

            elif column == self._TAGGER:
                return training.get_tagger()

            elif column == self._MAX_INDIV_SIZE:
                return training.get_max_indiv_size()

            elif column == self._STATE:
                return training.get_state()

            if role == QtCore.Qt.ToolTipRole:
                return "Training with corpus {0} and tagger {1}".format(training.get_corpus(), training.get_tagger())

    def flags(self, index=QtCore.QModelIndex):
        return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled
