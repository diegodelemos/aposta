try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtCore
except ImportError:
    from PySide import QtCore


class TrendListModel(QtCore.QAbstractListModel):

    def __init__(self, trends=None, parent=None):
        QtCore.QAbstractListModel.__init__(self, parent)
        self._trends = trends or []
        self._header = "Training individuals"
        self._state = dict()
        for trend in trends:
            self._state[trend] = QtCore.Qt.Unchecked

    def set_trends(self, trends):
        self._trends = trends

    def uncheck_all(self):
        for index, trend in enumerate(self._state.keys()):
            self._state[trend] = QtCore.Qt.Unchecked
            self.dataChanged.emit(QtCore.QModelIndex(), QtCore.QModelIndex())

    def get_checked_trends(self):
        return [trend
                for trend in self._trends
                if self._state[trend] == QtCore.Qt.Checked]

    def rowCount(self, parent=None):
        return len(self._trends)

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self._header

    def data(self, index, role=QtCore.Qt.DisplayRole):

        trend = self._trends[index.row()]

        if role == QtCore.Qt.UserRole:
            return trend
        
        if role == QtCore.Qt.DisplayRole:
            return trend.get_individual_number()

        if role == QtCore.Qt.ToolTipRole:
            return "individual"

        if role == QtCore.Qt.CheckStateRole:
            return QtCore.Qt.CheckState(self._state[trend])
            
    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        trend = self._trends[index.row()]

        if role == QtCore.Qt.CheckStateRole:
            self._state[trend] = value
            self.dataChanged.emit(index, index)
            return True

        return True

    def flags(self, index=QtCore.QModelIndex):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsUserCheckable
