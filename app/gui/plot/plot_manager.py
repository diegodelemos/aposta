class PlotManager:

    def get_widget_container(self):
        raise NotImplementedError

    def save_plot(self, filename):
        raise NotImplementedError

    def set_title(self, title):
        raise NotImplementedError

    def draw_plots(self):
        raise NotImplementedError

    def clear_plots(self):
        raise NotImplementedError

    def add_continuous_plot(self, label, x_values, y_values):
        raise NotImplementedError

    def add_discrete_plot(self, label, x_values, y_values):
        raise NotImplementedError
