import matplotlib
    # matplotlib needs to know which type are the UI components
    # in order to create the right and compatible canvas
    # for the figures
try:
    from PyQt4 import QtGui

    matplotlib.rcParams['backend.qt4'] = 'PyQt4'
except ImportError:
    from PySide import QtGui
    matplotlib.rcParams['backend.qt4'] = 'PySide'

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from app.gui.plot.plot_manager import PlotManager


class QtMPLManager(PlotManager):
    def __init__(self, gui_parent):
        self.mpl_figure_canvas = FigureCanvas(Figure(figsize=(10, 4), dpi=100))
        self.mpl_figure_canvas.setParent(gui_parent)
        self.axes = self.mpl_figure_canvas.figure.add_subplot(111)
        self.title = ""
        self.mpl_toolbar = NavigationToolbar(self.mpl_figure_canvas, gui_parent)
        self._clean_toolbar()
        #self.mpl_figure_canvas.mpl_connect('pick_event', self._update_legend)
        self.plots = list()
        # plot canvas size
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        self.mpl_figure_canvas.setSizePolicy(size_policy)

    def save_plot(self, filename):
        self.mpl_figure_canvas.figure.savefig(filename)

    def set_title(self, title):
        self.axes.set_title(title)
        self.mpl_figure_canvas.title = title
        self.mpl_figure_canvas.draw()

    def draw_plots(self):
        self.axes.set_title(self.mpl_figure_canvas.title)
        if self.plots:
            self._show_legend()

        self.mpl_figure_canvas.draw()

    def clear_plots(self):
        self.plots = list()
        self.axes.clear()
        self.axes.set_title(self.mpl_figure_canvas.title)
        self.mpl_figure_canvas.draw()

    def get_toolbar(self):
        return self.mpl_toolbar

    def add_continous_plot(self, label, x_values, y_values):
        plot, = self.axes.plot(
            x_values,
            y_values,
            label=label,
        )

        self.plots.append(plot)

    def add_discrete_plot(self, label, x_values, y_values):
        plot, = self.axes.plot(
            x_values,
            y_values,
            label=label,
            marker=".",
            linestyle='None'
        )

        self.plots.append(plot)

    def get_widget_container(self):
        return self.mpl_figure_canvas

    def _show_legend(self):
        leg = self.axes.legend(loc='upper left')
        leg.get_frame().set_alpha(0.4)
        leg.draggable()
        """
        self.mpl_figure_canvas.lined = dict()
        for legend_line, origin_line in zip(leg.get_lines(), self.plots):
            legend_line.set_picker(5)  # 5 pts tolerance
            self.mpl_figure_canvas.lined[legend_line] = origin_line

        """

    def _update_legend(self, event):
        # on the pick event, find the original line corresponding to the
        # legend proxy line, and toggle the visibility
        legend_line = event.artist
        original_line = self.mpl_figure_canvas.lined[legend_line]
        vis = not original_line.get_visible()
        original_line.set_visible(vis)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled
        if vis:
            legend_line.set_alpha(1.0)
        else:
            legend_line.set_alpha(0.2)

        self.mpl_figure_canvas.draw()

    def _clean_toolbar(self):
        actions = self.mpl_toolbar.findChildren(QtGui.QAction)
        for a in actions:
            if a.text() in ['Subplots', 'Save', 'Customize']:
                self.mpl_toolbar.removeAction(a)
