try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtGui, QtCore
except ImportError:
    from PySide import QtGui, QtCore

from app.gui.views.windows.plot_window import PlotWindow


class PlotView(PlotWindow):
    def __init__(self, model, ctrl, training, parent=None):
        PlotWindow.__init__(self, parent)

        self.trend_list_model = model
        self.plot_controller = ctrl
        self.training = training
        self.current_discrete_plots = dict()
        self.current_plot_trends = list()

        self.trend_list_view.setModel(self.trend_list_model)
        self.plot_provider.set_title(self.training.get_tagger())

        if training.get_anchor() == "no-anchor":
            self.residuals_inf_checkbox.hide()

        self.setup_event_bindings()

    def setup_event_bindings(self):
        self.select_plot_type.currentIndexChanged.connect(self.plot_type_switch)
        self.close_window.clicked.connect(self.close)
        self.draw_button.clicked.connect(self.on_draw)
        self.clear_button.clicked.connect(self.on_clear)
        self.save_text_action.triggered.connect(self.on_save_as_text)
        self.save_image_action.triggered.connect(self.on_save_as_image)

    def on_draw(self):
        self.plot_provider.clear_plots()
        self.plot_provider.set_title(self.training.get_tagger())
        self.current_discrete_plots.clear()
        self.current_plot_trends = list()
        if self.derivatives_checkbox.isChecked():
            self.plot_derivatives()

        if self.point_cloud_checkbox.isChecked():
            self.plot_point_cloud()

        if self.residuals_inf_checkbox.isChecked():
            self.plot_residuals_inf()

        if self.residuals_sum_checkbox.isChecked():
            self.plot_residuals_sum()

        if self.residuals_diff_same_checkbox.isChecked():
            self.plot_residuals_diff_same_individual()

        if self.residuals_diff_prev_checkbox.isChecked():
            self.plot_residuals_diff_prev_individual()

        if self.asymptotes_checkbox.isChecked():
            self.plot_asymptotes()

        self.plot_selected_trends()

        self.plot_provider.draw_plots()

    def on_clear(self):
        # clear plot
        self.plot_provider.clear_plots()
        self.current_discrete_plots.clear()
        self.current_plot_trends = list()
        # uncheck all plot checkboxes
        self.trend_list_model.uncheck_all()
        self.asymptotes_checkbox.setCheckState(QtCore.Qt.Unchecked)
        self.derivatives_checkbox.setCheckState(QtCore.Qt.Unchecked)
        self.point_cloud_checkbox.setCheckState(QtCore.Qt.Unchecked)
        self.residuals_diff_prev_checkbox.setCheckState(QtCore.Qt.Unchecked)
        self.residuals_diff_same_checkbox.setCheckState(QtCore.Qt.Unchecked)
        self.residuals_inf_checkbox.setCheckState(QtCore.Qt.Unchecked)
        self.residuals_sum_checkbox.setCheckState(QtCore.Qt.Unchecked)

    def on_save_as_text(self):
        import os
        import datetime
        res = QtGui.QFileDialog.getSaveFileName(
            self,
            caption="Text file",
            directory="{}.txt".format(datetime.datetime.now()),
            filter="Text files (*.txt)"
        )
        # it is made that way in order to be compatible with both qt apis PyQT4 and PySide
        if type(res) is tuple:
            filename = res[0]
        else:
            filename = res

        if filename:
            self.plot_controller.save_data_to_text_file(
                filename,
                self.current_discrete_plots
            )

            if os.path.exists(filename):
                QtGui.QMessageBox.information(self,
                                              "Success",
                                              "The data has been successfully saved to the file {}".format(filename)
                                              )
            else:
                QtGui.QMessageBox.warning(self,
                                          "Error",
                                          "There was an error while saving the file"
                                          )

    def on_save_as_image(self):
        import os
        import datetime
        res = QtGui.QFileDialog.getSaveFileName(
            self,
            caption="Image file",
            directory="{}.png".format(datetime.datetime.now()),
            filter="Images (*.png *.svg *.jpg)",
        )
        # it is made that way in order to be compatible with both qt apis PyQT4 and PySide
        if type(res) is tuple:
            filename = res[0]
        else:
            filename = res

        if filename:
            self.plot_provider.save_plot(filename)
            if os.path.exists(filename):
                QtGui.QMessageBox.information(self,
                                              "Success",
                                              "The data has been successfully saved to the file {}".format(filename)
                                              )
            else:
                QtGui.QMessageBox.warning(self,
                                          "Error",
                                          "There was an error while saving the file"
                                          )

    def plot_selected_trends(self):
        for trend_number, trend_function in self.plot_controller.get_selected_trends_plot_data():
            self.plot_provider.add_continous_plot(trend_number, self.training.get_training_range(), trend_function)

    def plot_derivatives(self):
        values, derivatives = self.plot_controller.get_trend_derivatives_data()
        label = "Derivatives"
        self.current_discrete_plots[label] = (values, derivatives)
        return self.plot_provider.add_continous_plot(label, values, derivatives)

    def plot_point_cloud(self):
        x, y = self.plot_controller.get_trend_point_cloud_data()
        label = "Point cloud"
        self.current_discrete_plots[label] = (x, y)
        return self.plot_provider.add_discrete_plot(label, x, y)

    def plot_residuals_inf(self):
        values, residuals = self.plot_controller.get_residuals_inf()
        label = "Residuals-inf"
        self.current_discrete_plots[label] = (values, residuals)
        return self.plot_provider.add_continous_plot(label, values, residuals)

    def plot_residuals_sum(self):
        values, residuals_sums = self.plot_controller.get_residuals_sum()
        label = "Residuals-sum"
        self.current_discrete_plots[label] = (values, residuals_sums)
        return self.plot_provider.add_continous_plot(label, values, residuals_sums)

    def plot_residuals_diff_same_individual(self):
        values, residuals_diffs = self.plot_controller.get_residuals_diff_same_individual()
        label = "Residuals-diff-same"
        self.current_discrete_plots[label] = (values, residuals_diffs)
        return self.plot_provider.add_continous_plot(label, values, residuals_diffs)

    def plot_residuals_diff_prev_individual(self):
        values, residuals_diffs = self.plot_controller.get_residuals_diff_prev_individual()
        label = "Residuals-diff-prev"
        self.current_discrete_plots[label] = (values, residuals_diffs)
        return self.plot_provider.add_continous_plot(label, values, residuals_diffs)

    def plot_asymptotes(self):
        values, asymptotes = self.plot_controller.get_asymptote_plot_data()
        label = "Asymptotes"
        self.current_discrete_plots[label] = (values, asymptotes)
        return self.plot_provider.add_continous_plot(label, values, asymptotes)

    def plot_type_switch(self):
        group_num = self.select_plot_type.currentIndex()
        for pos, group in enumerate(self.plot_groups):
            if group_num == pos:
                group.setHidden(False)
                continue

            group.setHidden(True)

        self.on_clear()

    def closeEvent(self, event):
        self.plot_controller.delete_plot_manager(self)
        event.accept()
