try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtGui, QtCore
except ImportError:
    from PySide import QtGui, QtCore

from app import config
from app.gui.views.windows.run_training_window import RunTrainingWindow


class RunTrainingView(RunTrainingWindow):

    def __init__(self, ctrl, parent=None):
        RunTrainingWindow.__init__(self, parent)
        self.ctrl = ctrl
        self.set_form_validators()
        self.setup_event_bindings()

    def set_form_validators(self):
        nu_validator = QtGui.QDoubleValidator(0, 1, 6)
        self.nu_edit.setValidator(nu_validator)

        varsigma_validator = QtGui.QDoubleValidator(1, 1000, 6)
        self.varsigma_edit.setValidator(varsigma_validator)

        lambda_validator = QtGui.QIntValidator(1, 1000)
        self.lambda_edit.setValidator(lambda_validator)

        kernel_length_validator = QtGui.QIntValidator(1, 20000)
        self.kernel_length_edit.setValidator(kernel_length_validator)

        convergence_layer_validator = QtGui.QDoubleValidator(1, 20000, 6)
        self.convergence_threshold_edit.setValidator(convergence_layer_validator)

        arithmetic_step_validator = QtGui.QIntValidator(1, 10000)
        self.arithmetic_step_edit.setValidator(arithmetic_step_validator)

        geometric_factor_validator = QtGui.QDoubleValidator(1, 100, 6)
        self.geometric_factor_edit.setValidator(geometric_factor_validator)

        adaptative_port_validator = QtGui.QDoubleValidator(0, 1, 6)
        self.adaptive_port_edit.setValidator(adaptative_port_validator)

        max_indiv_validator = QtGui.QIntValidator(1, 8000000)
        self.max_indiv_edit.setValidator(max_indiv_validator)

    def setup_event_bindings(self):
        self.run_test_button.clicked.connect(self.run_test)
        self.close_window.clicked.connect(self.close_actual_window)
        self.select_step_config_action.triggered.connect(self.switch_step_options)

        self.user_manual_action.triggered.connect(self.show_user_manual)
        self.about_action.triggered.connect(self.show_about)
        self.view_results_action.triggered.connect(self.show_results_windows)

    def show_results_windows(self):
        self.ctrl.show_training_list_view(self)

    def switch_step_options(self):
        option = self.select_step_config_action.checkedAction().text()
        if option == 'Arithmetic':
            self.arithmetic_step_edit.setHidden(False)
            self.arithmetic_step_label.setHidden(False)
            self.geometric_factor_edit.setHidden(True)
            self.geometric_factor_edit.clear()
            self.geometric_factor_label.setHidden(True)
            self.adaptive_port_edit.setHidden(True)
            self.adaptive_port_edit.clear()
            self.adaptive_port_label.setHidden(True)
        elif option == 'Geometric':
            self.arithmetic_step_edit.setHidden(True)
            self.arithmetic_step_edit.clear()
            self.arithmetic_step_label.setHidden(True)
            self.geometric_factor_edit.setHidden(False)
            self.geometric_factor_label.setHidden(False)
            self.adaptive_port_edit.setHidden(True)
            self.adaptive_port_edit.clear()
            self.adaptive_port_label.setHidden(True)
        elif option == 'Arithmetic and Geometric':
            self.arithmetic_step_edit.setHidden(False)
            self.arithmetic_step_label.setHidden(False)
            self.geometric_factor_edit.setHidden(False)
            self.geometric_factor_label.setHidden(False)
            self.adaptive_port_edit.setHidden(True)
            self.adaptive_port_edit.clear()
            self.adaptive_port_label.setHidden(True)
        elif option == 'Adaptive(arithmetic)':
            self.arithmetic_step_edit.setHidden(False)
            self.arithmetic_step_label.setHidden(False)
            self.geometric_factor_edit.setHidden(True)
            self.geometric_factor_edit.clear()
            self.geometric_factor_label.setHidden(True)
            self.adaptive_port_edit.setHidden(False)
            self.adaptive_port_label.setHidden(False)
        elif option == 'Adaptive(geometric)':
            self.arithmetic_step_edit.setHidden(True)
            self.arithmetic_step_edit.clear()
            self.arithmetic_step_label.setHidden(True)
            self.geometric_factor_edit.setHidden(False)
            self.geometric_factor_label.setHidden(False)
            self.adaptive_port_edit.setHidden(False)
            self.adaptive_port_label.setHidden(False)

    def show_user_manual(self):
        QtGui.QMessageBox.information(
            self,
            "User Manual",
            "Here you can browse the user manual"
        )

    def show_about(self):
        QtGui.QMessageBox.about(
            self,
            "APOSTA 0.1",
            "Graphic tool for scientific visualization."
        )
        
    def run_test(self):
        if self._check_form():
            num_process, ok = QtGui.QInputDialog.getInt(self,
                                                        "Parallel process",
                                                        "Number of parallel process:",
                                                        1,
                                                        1,
                                                        10)
            if ok:
                QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
                ok, value = self.ctrl.run_test(
                    self.anchor.currentText(),
                    self.nu_edit.text(),
                    self.varsigma_edit.text(),
                    self.lambda_edit.text(),
                    self.kernel_length_edit.text(),
                    self.arithmetic_step_edit.text(),
                    self.geometric_factor_edit.text(),
                    self.adaptive_port_edit.text(),
                    self.convergence_threshold_edit.text(),
                    self.corpus.currentText(),
                    self.tagger.currentText(),
                    self.max_indiv_edit.text(),
                    num_process
                )
                QtGui.QApplication.restoreOverrideCursor()

                if ok:
                    title = "Training launched successfully"
                    text = "Training id: {id}".format(id=value.get_id())
                    QtGui.QMessageBox.information(self, title, text)
                else:
                    title = "There was an error during training execution"
                    text = "{}".format(value)
                    QtGui.QMessageBox.warning(self, title, text)

        else:
            title = "Incomplete fields"
            text = "It seems that you forgot some parameter(s)."
            QtGui.QMessageBox.warning(self, title, text)

    def _check_form(self):
        ok = True

        if ((not self.nu_edit.text()) or
                (not self.varsigma_edit.text()) or
                (not self.lambda_edit.text()) or
                (not self.max_indiv_edit.text()) or
                (not self.kernel_length_edit.text()) or
                (not self.convergence_threshold_edit.text())):
            ok = False
        option = self.select_step_config_action.checkedAction().text()
        if option == 'Arithmetic':
            if not self.arithmetic_step_edit.text():
                ok = False
        elif option == 'Geometric':
            if not self.geometric_factor_edit.text():
                ok = False
        elif option == 'Arithmetic and Geometric':
            if not (self.geometric_factor_edit.text() and self.arithmetic_step_edit.text()):
                ok = False
        elif option == 'Adaptive(arithmetic)':
            if not (self.adaptive_port_edit.text() and self.arithmetic_step_edit.text()):
                ok = False
        elif option == 'Adaptive(geometric)':
            if not (self.adaptive_port_edit.text() and self.geometric_factor_edit.text()):
                ok = False
        return ok

    def close_actual_window(self):
        self.close()
