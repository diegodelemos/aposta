try:
    import sip
    sip.setapi('QVariant', 2)
    sip.setapi('QString', 2)
    from PyQt4 import QtCore, QtGui
except ImportError:
    from PySide import QtCore, QtGui

from app.gui.views.windows.training_list_window import TrainingListWindow


class TrainingListView(TrainingListWindow):
    
    def __init__(self, model, ctrl, parent=None):

        TrainingListWindow.__init__(self, parent)

        # now, we create a proxy_model to enable sorting and search
        self.trainings_model = model
        self.trainings_ctrl = ctrl
        self.trainings_table.setModel(self.trainings_model)
        # adjust names to cells

        self.trainings_table.setSortingEnabled(True)

        self.setup_event_bindings()
        self.trainings_table.setFocus()

    def setup_event_bindings(self):
        self.dump_button.clicked.connect(self.dump_dirs)
        self.delete_all.clicked.connect(self.delete_all_trainings)
        self.delete_test_button.clicked.connect(self.delete_training)
        self.refresh_button.clicked.connect(self.refresh_trainings)
        self.consult_test_button.clicked.connect(self.consult_training)
        self.close_window.clicked.connect(self.close_actual_window)

        self.id_text.textChanged.connect(lambda: self.trainings_model.set_filter_id(self.id_text.text()))
        self.corpus_text.textChanged.connect(lambda: self.trainings_model.set_filter_corpus(self.corpus_text.text()))
        self.tagger_text.textChanged.connect(lambda: self.trainings_model.set_filter_tagger(self.tagger_text.text()))
        self.anchor_text.textChanged.connect(lambda: self.trainings_model.set_filter_anchor(self.anchor_text.text()))
        self.kernel_text.textChanged.connect(lambda: self.trainings_model.set_filter_kernel(self.kernel_text.text()))
        self.convergence_threshold_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_convergence_threshold(self.convergence_threshold_text.text())
        )
        self.verticality_threshold_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_verticality_threshold(self.verticality_threshold_text.text())
        )
        self.slow_down_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_slow_down(self.slow_down_text.text())
        )
        self.lookahead_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_lookahead(self.lookahead_text.text())
        )
        self.arithmetic_step_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_arithmetic_step(self.arithmetic_step_text.text())
        )
        self.geometric_factor_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_geometric_factor(self.geometric_factor_text.text())
        )
        self.adaptive_port_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_adaptive_port(self.adaptive_port_text.text())
        )
        self.max_indiv_size_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_max_indiv_size(self.max_indiv_size_text.text())
        )
        self.state_text.textChanged.connect(
            lambda: self.trainings_model.set_filter_state(self.state_text.text())
        )

        self.trainings_table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.trainings_table.customContextMenuRequested.connect(self.launch_training_context_menu)

    def consult_training(self):
        training = list(self._get_selected_trainings())[0]
        if training.get_state() == 'started':
            QtGui.QMessageBox.warning(
                self,
                "Training {} still not ended".format(training.get_id()),
                "Wait until it is ended to consult it."
            )
        elif training.get_state() == 'No':
            QtGui.QMessageBox.warning(
                self,
                "Training {} did not end correctly".format(training.get_id()),
                "Try to rerun it."
            )
        else:
            self.trainings_ctrl.consult_training(training)

    def delete_training(self):
        training = list(self._get_selected_trainings())[0]

        msg = "You are going to delete the following trainings training:\n{}\nAre you sure?".format(training.get_id())
        msg_box = QtGui.QMessageBox()
        msg_box.setWindowTitle("Delete confirmation")
        msg_box.setText(msg)
        msg_box.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        msg_box.exec_()
        if msg_box.clickedButton().text() == "&Yes":
            QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.trainings_ctrl.delete_training(training)
            QtGui.QApplication.restoreOverrideCursor()

    def dump_dirs(self):
        QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        ok, msg = self.trainings_ctrl.dump_dirs_to_db()
        QtGui.QApplication.restoreOverrideCursor()
        if ok:
            title = "Dump dirs"
            text = "{}".format(msg)
            QtGui.QMessageBox.information(self, title, text)
        else:
            title = "There was an error during the dump."
            text = "{}".format(msg)
            QtGui.QMessageBox.warning(self, title, text)

    def delete_all_trainings(self):
        msg = "You are about to delete all, are you sure?"
        msg_box = QtGui.QMessageBox()
        msg_box.setWindowTitle("Delete confirmation")
        msg_box.setText(msg)
        msg_box.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        msg_box.exec_()
        if msg_box.clickedButton().text() == "&Yes":
            QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            ok, msg = self.trainings_ctrl.delete_all()
            QtGui.QApplication.restoreOverrideCursor()
            if ok:
                title = "Delete all"
                text = "{}".format(msg)
                QtGui.QMessageBox.information(self, title, text)
            else:
                title = "There was an error while deleting all runs."
                text = "{}".format(msg)
                QtGui.QMessageBox.warning(self, title, text)

    def refresh_trainings(self):
        QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        ok, msg = self.trainings_ctrl.refresh_trainings()
        QtGui.QApplication.restoreOverrideCursor()
        if not ok:
            title = "Refresh training list"
            text = "{}".format(msg)
            QtGui.QMessageBox.information(self, title, text)
        else:
            self.trainings_table.resizeColumnsToContents()
            self.trainings_table.resizeRowsToContents()

    def launch_training_context_menu(self, point):
        menu = QtGui.QMenu()
        consult_training = menu.addAction("Consult")
        rerun_training = menu.addAction("Re-run")
        delete_training = menu.addAction("Delete")
        menu.addSeparator()
        refresh_trainings = menu.addAction("Refresh trainings")
        consult_training.triggered.connect(self.consult_training)
        rerun_training.triggered.connect(self.rerun_training)
        delete_training.triggered.connect(self.delete_training)
        refresh_trainings.triggered.connect(self.refresh_trainings)
        menu.exec_(self.mapToGlobal(point))

    def rerun_training(self):
        training = list(self._get_selected_trainings())[0]

        if training.get_state() == 'started':
            QtGui.QMessageBox.warning(
                self,
                "Training {} still not ended".format(training.get_id()),
                "Wait until it is ended to re-run it."
            )
        elif training.get_state() != 'No':
            QtGui.QMessageBox.warning(
                self,
                "Training {} ended correctly".format(training.get_id()),
                "So there is no point to re-run it."
            )
        else:
            num_process, ok = QtGui.QInputDialog.getInt(self,
                                                    "Parallel process",
                                                    "Number of parallel process to rerun training {}:".format(training.get_id()),
                                                    1,
                                                    1,
                                                    100)

        if ok:
            QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.trainings_ctrl.rerun_training(training, num_process)
            QtGui.QApplication.restoreOverrideCursor()

    def _get_selected_trainings(self):
        trainings = list()
        for index in self.trainings_table.selectedIndexes():
            trainings.append(self.trainings_model.data(index, QtCore.Qt.UserRole))

        # at this point we could have repeated trainings,
        # so I pass the list to the set constructor to remove
        # repeated items
        # TODO fix the previous list to only contain unique items
        # so the performance gets improved
        return set(trainings)

    def close_actual_window(self):
        self.close()
