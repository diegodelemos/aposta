try:
    from PyQt4 import QtGui
except ImportError:
    from PySide import QtGui

from app.gui.plot.qt_mpl_manager import QtMPLManager


class PlotWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setWindowTitle('Plot manager')
        self.resize(800, 450)
        screen_geometry = QtGui.QApplication.desktop().screenGeometry()
        x = (screen_geometry.width()-self.width()) / 2
        y = (screen_geometry.height()-self.height()) / 2
        self.move(x, y)
        self.plot_provider = QtMPLManager(self)
        self.init_ui()

    def init_ui(self):
        self.create_menu_bar()
        main_container = QtGui.QWidget()

        self.select_plot_type = QtGui.QComboBox(self)
        self.select_plot_type.addItem("Trends plot")
        self.select_plot_type.addItem("Derivatives plot")
        self.select_plot_type.addItem("Asymptotes plot")
        self.select_plot_type.addItem("Residuals plot")


        plot_v_layout = QtGui.QVBoxLayout()
        plot_v_layout.addWidget(self.plot_provider.get_widget_container())
        plot_v_layout.addWidget(self.plot_provider.get_toolbar())
        # Trend related group
        self.trend_plot_group = QtGui.QGroupBox("Trend related plots")
        self.trend_plot_group.setHidden(False)
        trend_plot_v_layout = QtGui.QVBoxLayout()
        self.point_cloud_checkbox = QtGui.QCheckBox("Point cloud:", self)
        self.trend_list_label = QtGui.QLabel("Trend list:")
        self.trend_list_view = QtGui.QListView()
        trend_plot_v_layout.addWidget(self.point_cloud_checkbox)
        trend_plot_v_layout.addWidget(self.trend_list_label)
        trend_plot_v_layout.addWidget(self.trend_list_view)
        self.trend_plot_group.setLayout(trend_plot_v_layout)

        # Derivatives related group
        self.derivative_plot_group = QtGui.QGroupBox("Derivatives plots")
        self.derivative_plot_group.setHidden(True)
        derivative_plot_v_layout = QtGui.QVBoxLayout()
        self.derivatives_checkbox = QtGui.QCheckBox("Derivatives:", self)
        derivative_plot_v_layout.addWidget(self.derivatives_checkbox)
        self.derivative_plot_group.setLayout(derivative_plot_v_layout)

        # Asymptotes related group
        self.asymptotes_plot_group = QtGui.QGroupBox("Asymptotes plots")
        self.asymptotes_plot_group.setHidden(True)
        asymptotes_plot_v_layout = QtGui.QVBoxLayout()
        self.asymptotes_checkbox = QtGui.QCheckBox("Asymptotes:", self)
        asymptotes_plot_v_layout.addWidget(self.asymptotes_checkbox)
        self.asymptotes_plot_group.setLayout(asymptotes_plot_v_layout)

        # Residuals related group
        self.residuals_plot_group = QtGui.QGroupBox("Residuals plots")
        self.residuals_plot_group.setHidden(True)
        residuals_plot_v_layout = QtGui.QVBoxLayout()
        self.residuals_inf_checkbox = QtGui.QCheckBox("Residuals-inf:", self)
        self.residuals_sum_checkbox = QtGui.QCheckBox("Residuals-sum:", self)
        self.residuals_diff_same_checkbox = QtGui.QCheckBox("Residuals-diff-same-indiv:", self)
        self.residuals_diff_prev_checkbox = QtGui.QCheckBox("Residuals-diff-prev-indiv:", self)
        residuals_plot_v_layout.addWidget(self.residuals_inf_checkbox)
        residuals_plot_v_layout.addWidget(self.residuals_sum_checkbox)
        residuals_plot_v_layout.addWidget(self.residuals_diff_same_checkbox)
        residuals_plot_v_layout.addWidget(self.residuals_diff_prev_checkbox)
        self.residuals_plot_group.setLayout(residuals_plot_v_layout)

        # plot groups list
        self.plot_groups = [self.trend_plot_group,
                            self.derivative_plot_group,
                            self.asymptotes_plot_group,
                            self.residuals_plot_group]

        # Plot controls
        control_buttons_h_layout = QtGui.QHBoxLayout()
        self.draw_button = QtGui.QPushButton("&Draw")
        self.clear_button = QtGui.QPushButton("&Clear")
        self.close_window = QtGui.QPushButton("Close")
        control_buttons_h_layout.addWidget(self.draw_button)
        control_buttons_h_layout.addWidget(self.clear_button)
        control_buttons_h_layout.addSpacing(20)
        control_buttons_h_layout.addWidget(self.close_window)
        # Building layout
        plot_manager_v_layout = QtGui.QVBoxLayout()
        plot_manager_v_layout.addWidget(self.select_plot_type)
        plot_manager_v_layout.addWidget(self.trend_plot_group)
        plot_manager_v_layout.addWidget(self.derivative_plot_group)
        plot_manager_v_layout.addWidget(self.asymptotes_plot_group)
        plot_manager_v_layout.addWidget(self.residuals_plot_group)
        plot_manager_v_layout.addLayout(control_buttons_h_layout)
        options_container = QtGui.QWidget()
        options_container.setLayout(plot_manager_v_layout)

        # Make sure that the options_container doesn't take more width thant it needs.
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Maximum)
        options_container.setSizePolicy(size_policy)

        main_h_layout = QtGui.QHBoxLayout()
        main_h_layout.addLayout(plot_v_layout)
        main_h_layout.addWidget(options_container)
        main_container.setLayout(main_h_layout)
        self.setCentralWidget(main_container)

    def create_menu_bar(self):
        file_menu = self.menuBar().addMenu("&File")
        self.save_image_action = QtGui.QAction('Save as image', file_menu)
        self.save_text_action = QtGui.QAction('Save as plain text file', file_menu)
        file_menu.addAction(self.save_image_action)
        file_menu.addAction(self.save_text_action)

