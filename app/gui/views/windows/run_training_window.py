try:
    from PyQt4 import QtGui, QtCore
except ImportError:
    from PySide import QtGui, QtCore

from app import config


class RunTrainingWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):

        QtGui.QMainWindow.__init__(self, parent)
        self.setWindowTitle('gAPOSTA')
        #self.setFixedSize(600, 300)
        screen_geometry = QtGui.QApplication.desktop().screenGeometry()
        x = (screen_geometry.width()-self.width()) / 2
        y = (screen_geometry.height()-self.height()) / 2
        self.move(x, y)
        self.initUI()

    def initUI(self):
        self.widget = QtGui.QWidget()
        # menu creation
        self.create_menus()
        self.create_form()

        # show
        self.setCentralWidget(self.widget)

    def create_menus(self):
        self.step_menu = self.menuBar().addMenu("&Step")
        self.help_menu = self.menuBar().addMenu("&Help")
        self.view_results_menu = self.menuBar().addMenu("&Results")

        self.select_step_config_action = QtGui.QActionGroup(self)
        self.arithmetic_action = QtGui.QAction('Arithmetic', self.step_menu)
        self.arithmetic_action.setCheckable(True)
        self.select_step_config_action.addAction(self.arithmetic_action)
        self.geometric_action = QtGui.QAction('Geometric', self.step_menu)
        self.geometric_action.setCheckable(True)
        self.select_step_config_action.addAction(self.geometric_action)
        self.arithmetic_geometric_action = QtGui.QAction('Arithmetic and Geometric', self.step_menu)
        self.arithmetic_geometric_action.setCheckable(True)
        self.select_step_config_action.addAction(self.arithmetic_geometric_action)
        self.arithmetic_adaptive_action = QtGui.QAction('Adaptive(arithmetic)', self.step_menu)
        self.arithmetic_adaptive_action.setCheckable(True)
        self.select_step_config_action.addAction(self.arithmetic_adaptive_action)
        self.geometric_adaptive_action = QtGui.QAction('Adaptive(geometric)', self.step_menu)
        self.geometric_adaptive_action.setCheckable(True)
        self.select_step_config_action.addAction(self.geometric_adaptive_action)
        self.arithmetic_action.setChecked(True)
        self.step_menu.addAction(self.arithmetic_action)
        self.step_menu.addAction(self.geometric_action)
        self.step_menu.addAction(self.arithmetic_geometric_action)
        self.step_menu.addAction(self.arithmetic_adaptive_action)
        self.step_menu.addAction(self.geometric_adaptive_action)

        self.user_manual_action = QtGui.QAction('User Manual', self.help_menu)
        self.about_action = QtGui.QAction('About', self.help_menu)
        self.help_menu.addAction(self.user_manual_action)
        self.help_menu.addAction(self.about_action)

        self.view_results_action = QtGui.QAction('View Results', self.view_results_menu)
        self.view_results_menu.addAction(self.view_results_action)

    def create_form(self):
        self.grid = QtGui.QGridLayout()

        self.widget.setLayout(self.grid)

        # form elements
        self.corpus_label = QtGui.QLabel("Corpus:")
        self.corpus_label.setAlignment(QtCore.Qt.AlignRight)
        self.corpus = QtGui.QComboBox()
        self.corpus.addItems(config.available_corpus)


        self.tagger_label = QtGui.QLabel("Tagger:")
        self.tagger_label.setAlignment(QtCore.Qt.AlignRight)
        self.tagger = QtGui.QComboBox()
        self.tagger.addItems(config.available_taggers)

        self.anchor_label = QtGui.QLabel("Anchor:")
        self.anchor_label.setAlignment(QtCore.Qt.AlignRight)
        self.anchor = QtGui.QComboBox()
        self.anchor.addItems(config.available_anchors)

        self.nu_edit = QtGui.QLineEdit()
        nu = u"\u03BD"
        self.nu_label = QtGui.QLabel(nu)
        self.nu_label.setAlignment(QtCore.Qt.AlignRight)
        self.nu_label.setToolTip("Verticality threshold.")
        
        self.varsigma_edit = QtGui.QLineEdit()
        sigma = u"\u03C3"
        self.varsigma_label = QtGui.QLabel(sigma)
        self.varsigma_label.setAlignment(QtCore.Qt.AlignRight)
        self.varsigma_label.setToolTip("Slow down.")
        
        self.lambda_edit = QtGui.QLineEdit()
        lambda_ = u"\u03BB"
        self.lambda_label = QtGui.QLabel(lambda_)
        self.lambda_label.setAlignment(QtCore.Qt.AlignRight)
        self.lambda_label.setToolTip("Look ahead.")

        self.arithmetic_step_edit = QtGui.QLineEdit()
        self.arithmetic_step_label = QtGui.QLabel("Step:")
        self.arithmetic_step_label.setAlignment(QtCore.Qt.AlignRight)

        self.geometric_factor_edit = QtGui.QLineEdit()
        self.geometric_factor_edit.hide()
        self.geometric_factor_label = QtGui.QLabel("Factor:")
        self.geometric_factor_label.setAlignment(QtCore.Qt.AlignRight)
        self.geometric_factor_label.hide()

        self.adaptive_port_edit = QtGui.QLineEdit()
        self.adaptive_port_edit.hide()
        self.adaptive_port_label = QtGui.QLabel("PORT:")
        self.adaptive_port_label.setAlignment(QtCore.Qt.AlignRight)
        self.adaptive_port_label.hide()

        self.max_indiv_edit = QtGui.QLineEdit()
        self.max_indiv_label = QtGui.QLabel("Max. individual\nsize:")
        self.max_indiv_label.setAlignment(QtCore.Qt.AlignRight)
        
        self.kernel_length_edit = QtGui.QLineEdit()
        self.kernel_length_label = QtGui.QLabel("Kernel:")
        self.kernel_length_label.setAlignment(QtCore.Qt.AlignRight)

        self.convergence_threshold_edit = QtGui.QLineEdit()
        self.convergence_threshold_label = QtGui.QLabel("Convergence \nthreshold:")
        self.convergence_threshold_label.setAlignment(QtCore.Qt.AlignRight)

        # run button
        self.run_test_button = QtGui.QPushButton("Run test", self.widget)
        self.close_window = QtGui.QPushButton("Close", self.widget)

        # first row
        self.grid.addWidget(self.corpus_label, 0, 0)
        self.grid.addWidget(self.corpus, 0, 1)
        self.grid.addWidget(self.tagger_label, 0, 2)
        self.grid.addWidget(self.tagger, 0, 3)

        # first row
        self.grid.addWidget(self.anchor_label, 1, 0)
        self.grid.addWidget(self.anchor, 1, 1)
        self.grid.addWidget(self.nu_label, 1, 2)
        self.grid.addWidget(self.nu_edit, 1, 3)


        # second row
        self.grid.addWidget(self.kernel_length_label, 2, 0)
        self.grid.addWidget(self.kernel_length_edit, 2, 1)
        self.grid.addWidget(self.varsigma_label, 2, 2)
        self.grid.addWidget(self.varsigma_edit, 2, 3)

        # third row
        self.grid.addWidget(self.convergence_threshold_label, 3, 0)
        self.grid.addWidget(self.convergence_threshold_edit, 3, 1)
        self.grid.addWidget(self.lambda_label, 3, 2)
        self.grid.addWidget(self.lambda_edit, 3, 3)

        # fourth row
        self.grid.addWidget(self.max_indiv_label, 4, 0)
        self.grid.addWidget(self.max_indiv_edit, 4, 1)
        self.grid.addWidget(self.arithmetic_step_label, 4, 2)
        self.grid.addWidget(self.arithmetic_step_edit, 4, 3)

        # fifth row
        self.grid.addWidget(self.geometric_factor_label, 5, 0)
        self.grid.addWidget(self.geometric_factor_edit, 5, 1)
        # sixth row
        self.grid.addWidget(self.adaptive_port_label, 6, 0)
        self.grid.addWidget(self.adaptive_port_edit, 6, 1)
        # seventh row
        self.grid.addWidget(self.run_test_button, 7, 1)
        self.grid.addWidget(self.close_window, 7, 3)


