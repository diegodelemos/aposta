try:
    from PyQt4 import QtGui
except ImportError:
    from PySide import QtGui


class TrainingListWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        
        QtGui.QMainWindow.__init__(self, parent)
        self.setWindowTitle('gAPOSTA - View Results')
        self.resize(1320, 450)
        screen_geometry = QtGui.QApplication.desktop().screenGeometry()
        x = (screen_geometry.width()-self.width()) / 2
        y = (screen_geometry.height()-self.height()) / 2
        self.move(x, y)

        self.initUI()

    def initUI(self):

        self.widget = QtGui.QWidget()
        # horizontal box for searching
        self.search_hbox = QtGui.QHBoxLayout()
        self.id_text = QtGui.QLineEdit()
        self.id_text.setPlaceholderText("ID")
        self.corpus_text = QtGui.QLineEdit()
        self.corpus_text.setPlaceholderText("Corpus")
        self.tagger_text = QtGui.QLineEdit()
        self.tagger_text.setPlaceholderText("Tagger")
        self.anchor_text = QtGui.QLineEdit()
        self.anchor_text.setPlaceholderText("Anchor")
        self.kernel_text = QtGui.QLineEdit()
        self.kernel_text.setPlaceholderText("Kernel")
        self.convergence_threshold_text = QtGui.QLineEdit()
        self.convergence_threshold_text.setPlaceholderText("Convergence Threshold")
        self.verticality_threshold_text = QtGui.QLineEdit()
        self.verticality_threshold_text.setPlaceholderText("Verticality Threshold")
        self.slow_down_text = QtGui.QLineEdit()
        self.slow_down_text.setPlaceholderText("Slow down")
        self.lookahead_text = QtGui.QLineEdit()
        self.lookahead_text.setPlaceholderText("Lookahead")
        self.arithmetic_step_text = QtGui.QLineEdit()
        self.arithmetic_step_text.setPlaceholderText("Step")
        self.geometric_factor_text = QtGui.QLineEdit()
        self.geometric_factor_text.setPlaceholderText("Factor")
        self.adaptive_port_text = QtGui.QLineEdit()
        self.adaptive_port_text.setPlaceholderText("PORT")
        self.max_indiv_size_text = QtGui.QLineEdit()
        self.max_indiv_size_text.setPlaceholderText("Max. indiv. size")
        self.state_text = QtGui.QLineEdit()
        self.state_text.setPlaceholderText("State")

        self.search_hbox.addWidget(self.id_text)
        self.search_hbox.addWidget(self.corpus_text)
        self.search_hbox.addWidget(self.tagger_text)
        self.search_hbox.addWidget(self.anchor_text)
        self.search_hbox.addWidget(self.kernel_text)
        self.search_hbox.addWidget(self.convergence_threshold_text)
        self.search_hbox.addWidget(self.verticality_threshold_text)
        self.search_hbox.addWidget(self.slow_down_text)
        self.search_hbox.addWidget(self.lookahead_text)
        self.search_hbox.addWidget(self.arithmetic_step_text)
        self.search_hbox.addWidget(self.geometric_factor_text)
        self.search_hbox.addWidget(self.adaptive_port_text)
        self.search_hbox.addWidget(self.max_indiv_size_text)
        self.search_hbox.addWidget(self.state_text)
        # vertical box for training list and horizontal box (buttons)
        self.vbox = QtGui.QVBoxLayout()
        self.widget.setLayout(self.vbox)
        # horizontal box for buttons
        self.hbox = QtGui.QHBoxLayout()

        self.trainings_table = QtGui.QTableView()
        self.trainings_table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.trainings_table.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

        # actions
        self.dump_button = QtGui.QPushButton('Dump dirs')
        self.delete_all = QtGui.QPushButton('Delete all')
        self.refresh_button = QtGui.QPushButton('Refresh')
        self.delete_test_button = QtGui.QPushButton('Delete')
        self.consult_test_button = QtGui.QPushButton('Consult')
        self.close_window = QtGui.QPushButton('Close')

        
        # building up the layout
        self.vbox.addLayout(self.search_hbox)
        self.vbox.addWidget(self.trainings_table)
        self.vbox.addLayout(self.hbox)
        self.hbox.addWidget(self.dump_button)
        self.hbox.addWidget(self.delete_all)
        self.hbox.addStretch(1)
        self.hbox.addWidget(self.refresh_button)
        self.hbox.addWidget(self.delete_test_button)
        self.hbox.addWidget(self.consult_test_button)
        self.hbox.addSpacing(20)
        self.hbox.addWidget(self.close_window)
        self.setCentralWidget(self.widget)


