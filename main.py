import sys


def main():

    from app.gui.main import gAposta

    try:
        app = gAposta(sys.argv)
        app.start()
        sys.exit(app.exec_())
    except NameError:
        print("Name Error:", sys.exc_info()[1])
    except SystemExit:
        print("Closing Window...")
    except Exception:
        print(sys.exc_info()[1])
    finally:
        sys.exit(0)

    
if __name__ == "__main__":
    main()
